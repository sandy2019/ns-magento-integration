/*******************************************************************
 *
 * Name: lirik.utils.js
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @version: 2.0
 * 
 * Author: Lirik, Inc.(Sandeep Kumar) :: Date : 06/24/2021
 * Purpose: This library is used for the common functions.
 *
 * ******************************************************************* */

define(['N/record', 'N/search', 'N/format', 'N/config', 'N/email', 'N/runtime'], function (record, search, format, config, email, runtime) {
    return function () {

        /**
         *
         * @param {object} params 
         * @param {string} params.bufferHours 
         * @returns {string}
         */

        this.getFormattedDate = function (params) {
            try {
                var currentDate = new Date();
                currentDate.setHours(currentDate.getHours() - parseInt(params.hours));
                var date = currentDate.getDate();
                var month = currentDate.getMonth() + 1;
                var year = currentDate.getFullYear();

                //Note:: Get Time Details
                var hours = currentDate.getHours();
                var minutes = currentDate.getMinutes();
                var seconds = currentDate.getSeconds();

                //NOTE:: Add zero if month or date is less than 10
                if (month < 10) month = '0' + month;
                if (date < 10) date = '0' + date;

                //NOTE:: Add zero if hours, minutes and seconds are less than 10
                if (hours < 10) hours = '0' + hours;
                if (minutes < 10) minutes = '0' + minutes;
                if (seconds < 10) seconds = '0' + seconds;

                var strDate = year + '-' + month + '-' + date;
                var strTime = hours + ":" + minutes + ":" + seconds;
                var fullDateTime = strDate + " " + strTime;
                return fullDateTime;
            }
            catch (err) {
                log.error({ title: 'error getFormattedDate', details: JSON.stringify(err) });
                throw err;
            }

        };

        /**
    * NOTE:: DO NOT CHANGE ANY FUNCTIONS WITHOUT APPROVAL 
    * These methods handles the NetSuite API call internally for customer creation
    *
    * @param {object} params 
    * @param {object} params.fields
    * @param {string} params.actionType : ORDER
    * @param {string} params.eventType : create/change
    * @returns {string}
    */

        this.createSORecord = function (params) {

            let newOrderId = '0'; const title = 'createRecord';

            try {
                var nsRecordOrder = null;
                log.debug({ title, details: { params } });
                nsRecordOrder = record.create({
                    type: record.Type.SALES_ORDER,
                    isDynamic: true,
                    defaultValues: {
                        "customform": "115"
                    }
                });

                log.debug({ title, details: { nsRecordOrder } });

                setRecordFieldValues({ nsRecord: nsRecordOrder, fields: params.fields });
                log.debug({ title: "line2", details: { nsRecordOrder } });


                if (this.isNotEmptyAndhasOwnProperty(params, "lineItems")) {
                    const lineFields = params.lineItems;
                    log.debug({ title, details: { lineFields } });
                    if (lineFields && lineFields.length > 0) {

                        for (let line = 0; line < lineFields.length; line++) {

                            //log.debug({ title, details: { 'lineFields' : lineFields[line] } });
                            this.createSublistLine({
                                nsRecord: nsRecordOrder,
                                lineItem: 'item',
                                fields: lineFields[line]
                            });
                        }

                       // log.debug({ title: "shippingAddress", details: params.shippingAddress });
                        ///For Shipping and Billig Address
                        if (this.isNotEmptyAndhasOwnProperty(params, "shippingAddress")) {
                            this.createAddressSubrecord({
                                nsRecord: nsRecordOrder,
                                shippingAddress: params.shippingAddress
                            });
                        }
                        if (this.isNotEmptyAndhasOwnProperty(params, "billingAddress")) {
                            this.createAddressSubrecord({
                                nsRecord: nsRecordOrder,
                                billingAddress: params.billingAddress
                            });
                        }

                        //NOTE:: Gift Certificate
                        if (this.isNotEmptyAndhasOwnProperty(params, "giftCardLines")) {
                            const giftLineFields = params.giftCardLines;
                            log.debug({ title, details: { giftLineFields } });
                            if (giftLineFields && giftLineFields.length > 0) {
        
                                for (let line = 0; line < giftLineFields.length; line++) {
        
                                    //log.debug({ title, details: { 'lineFields' : lineFields[line] } });
                                    this.createSublistLine({
                                        nsRecord: nsRecordOrder,
                                        lineItem: 'giftcertredemption',
                                        fields: giftLineFields[line]
                                    });
                                }
                            }
                        }

                        // log.debug({ title, details: 'Address Validated' });
                        log.debug({ title, details: { tranid: params.fields.tranid } });
                        //if (!this.ns_order_number_exists({ orderNumber: params.fields.tranid })) 
                        {

                            newOrderId = nsRecordOrder.save({
                                ignoreMandatoryFields: true
                            });
                            log.debug({ title, details: { newOrderId } });

                        }
                        return newOrderId;

                    } else {
                        return false;
                    }
                }

            } catch (err) {
                log.error({ title: 'error ns_create_update_orders', details: JSON.stringify(err) });
                throw err;
            }
        };
        /**
        * @param {object} params 
        * @param {string} params.OrderId
        * @returns {boolean}
         */
        this.isOrderExistInNS = function (params) {
            try {
                let isExist = false;
                let salesorderSearchObj = search.create({
                    type: "salesorder",
                    filters: [
                        ["type", "anyof", "SalesOrd"],
                        "AND",
                        ["mainline", "is", "T"],
                        "AND",
                        ["custbody_magento_order_id", "is", params.OrderId.toString().trim()]
                    ],
                    columns: [
                        search.createColumn({ name: "internalid", label: "Internal ID" })
                    ]
                });
                let searchResultCount = salesorderSearchObj.runPaged().count;
                log.debug({ title: 'searchResultCount', details: searchResultCount });
                if (searchResultCount > 0)
                    isExist = true;
                return isExist;
            }
            catch (err) {
                log.error({
                    title: 'isOrderExistInNS::error',
                    details: err.message
                });
                //throw err;
            }
        };

        /**
  * Get Customer internal id of a record that have the same CT id.
  *
  * @param {Object} params
  * @returns {string} Internal Id of record.
  */
        this.nsCustomerId = function (params) {

            const title = 'nsCustomerId';

            try {

                log.debug({ title, details: { params } });

                if (params) {

                    let arrSeaFilters = [];
                    if (this.isNotEmptyAndhasOwnProperty(params, 'id')) {
                        arrSeaFilters.push(['custentity_magento_customer_id', search.Operator.IS, params.id]);
                    } else if (this.isNotEmptyAndhasOwnProperty(params, 'email')) {
                        arrSeaFilters.push(['email', search.Operator.IS, params.email]);//custentity_is_magento_customer
                        //arrSeaFilters.push(['custentity_is_magento_customer', search.Operator.IS, 'T']);//
                    }
                    let arrSeaColumns = [];
                    arrSeaColumns.push(search.createColumn({ name: 'internalid' }));

                    let nsSeaResultSet = search.create({ type: record.Type.CUSTOMER, filters: arrSeaFilters, columns: arrSeaColumns }).run();
                    let arrNSSeaResults = nsSeaResultSet.getRange({ start: 0, end: 1 });

                    if (Array.isArray(arrNSSeaResults) && arrNSSeaResults.length === 1) {
                        return arrNSSeaResults[0].id;
                    } else {
                        return '';
                    }

                }
            } catch (err) {
                log.error({ title: 'nsCustomerId', details: err });
                throw err;
            }
        };

        this.createRecord = function (params) {
            var nsRecordId = null;
            try {
                var nsRecordObj = null;
                nsRecordObj = record.create({ type: params.type, isDynamic: true, });
                setRecordFieldValues({ nsRecord: nsRecordObj, fields: params.fields });
                if (this.isNotEmptyAndhasOwnProperty(params, "lineItems")) {
                    const lineFields = params.lineItems;
                    log.debug({ title, details: { lineFields } });
                    if (lineFields && lineFields.length > 0) {

                        for (let line = 0; line < lineFields.length; line++) {

                            //log.debug({ title, details: { 'lineFields' : lineFields[line] } });
                            this.createSublistLine({
                                nsRecord: nsRecordObj,
                                lineItem: params.sublist,
                                fields: lineFields[line]
                            });
                        }
                    }
                }

                nsRecordId = nsRecordObj.save({
                    ignoreMandatoryFields: true
                });
                log.debug({ title: 'createRecord', details: { nsRecordId } });
                return nsRecordId;
            }
            catch (err) {
                log.error({ title: 'createRecord', details: err.message });
                return nsRecordId;
            }
        };

        this.createSublistLine = function (params) {
            const title = "createSublistLine";
            try {

                if (params.lineItem && params.fields) {
                    params.nsRecord.selectNewLine({
                        sublistId: params.lineItem
                    });

                    var fieldKeys = Object.keys(params.fields);
                    log.debug({ title, details: { fieldKeys } })
                    for (var numIndex = 0; numIndex < fieldKeys.length; numIndex++) {
                        if (params.fields[fieldKeys[numIndex]] !== undefined) {
                            if(fieldKeys[numIndex]=='authcode')
                            {
                                params.nsRecord.setCurrentSublistText({
                                    sublistId: params.lineItem,
                                    fieldId: fieldKeys[numIndex],
                                    text: params.fields[fieldKeys[numIndex]]
                                });
                            }
                            else{
                            params.nsRecord.setCurrentSublistValue({
                                sublistId: params.lineItem,
                                fieldId: fieldKeys[numIndex],
                                value: params.fields[fieldKeys[numIndex]]
                            });
                        }
                            //log.debug({title, details : {'value' : params.fields[fieldKeys[numIndex]]}});
                        }
                    }

                    params.nsRecord.commitLine({
                        sublistId: params.lineItem
                    });
                }
            } catch (err) {
                log.error({
                    title: 'createSublistLine',
                    details: err.message
                });
                //throw err;
            }
        }
        this.createAddressSubrecord = function (params) {
            const title = "ns_create_subrecord_address";
            try {

                let addrSubrecord = null; let addressJSON = null;
                if (this.isNotEmptyAndhasOwnProperty(params, "shippingAddress")) {
                    params.nsRecord.setValue({ fieldId: 'shipaddresslist', value: null });
                    addrSubrecord = params.nsRecord.getSubrecord({ fieldId: 'shippingaddress' });
                    addrSubrecord.setValue({ fieldId: 'isresidential', value: 'T' });
                    addressJSON = params.shippingAddress;
                    log.debug({ title, details: { 'shippingAddress': addressJSON } });
                }
                if (this.isNotEmptyAndhasOwnProperty(params, "billingAddress")) {
                    params.nsRecord.setValue({ fieldId: 'billaddresslist', value: null });
                    addrSubrecord = params.nsRecord.getSubrecord({ fieldId: 'billingaddress' });
                    addressJSON = params.billingAddress;
                    log.debug({ title, details: { 'billingAddress': addressJSON } })
                }

                if (addrSubrecord && addrSubrecord != null && addressJSON && addressJSON != null) {
                    setRecordFieldValues({ nsRecord: addrSubrecord, fields: addressJSON });

                }
            } catch (err) {
                log.error({
                    title: 'ns_create_subrecord_address',
                    details: err.message
                });
                //throw err;
            }
        }

        function setRecordFieldValues(params) {
            try {

                var fieldKeys = Object.keys(params.fields);

                for (var numIndex = 0; numIndex < fieldKeys.length; numIndex++) {
                    if (params.fields[fieldKeys[numIndex]] !== undefined) {
                        //FOR SPEACIAL CASE
                        if (params.fields[fieldKeys[numIndex]].toString().indexOf('%') > -1) {
                            // log.debug({ title: 'setRecordFieldValues', details: "shipmethod" })
                            params.nsRecord.setText({
                                fieldId: fieldKeys[numIndex],
                                text: params.fields[fieldKeys[numIndex]]
                            });
                        } else {
                            params.nsRecord.setValue({
                                fieldId: fieldKeys[numIndex],
                                value: params.fields[fieldKeys[numIndex]]
                            });
                        }

                    }
                }
            } catch (err) {
                log.error({ title: 'setRecordFieldValues', details: err });
                //throw err;
            }
        }

        this.customPreferences = function () {
            try {
                const _nsScriptRec = runtime.getCurrentScript();

                const MAGENTO_URL = _nsScriptRec.getParameter({ name: 'custscript_lirik_mr_magento_base_url' });
                const CONSUMER_KEY = _nsScriptRec.getParameter({ name: 'custscript_lirik_mr_magento_consumer_key' });
                const CONSUMER_SECRET = _nsScriptRec.getParameter({ name: 'custscript_lirik_mr_mgnto_cnsumer_secret' });
                const TOKEN_KEY = _nsScriptRec.getParameter({ name: 'custscript_lirik_mr_magento_token_key' });
                const TOKEN_SECRET = _nsScriptRec.getParameter({ name: 'custscript_lirik_mr_magento_token_secret' });
                const BUFFER_HOURS = _nsScriptRec.getParameter({ name: 'custscript_lirik_mr_magento_buffer_hours' });
                const TAX_CODE = '';
                const PAGE_SIZE = _nsScriptRec.getParameter({ name: 'custscript_lirik_mr_magento_page_size' });

                /* log.debug({ title: 'MAGENTO_URL', details: MAGENTO_URL });*/
                /* log.debug({ title: 'BUFFER_HOURS', details: BUFFER_HOURS });*/
                var configParams = { MAGENTO_URL: MAGENTO_URL, CONSUMER_KEY: CONSUMER_KEY, CONSUMER_SECRET: CONSUMER_SECRET, TOKEN_KEY: TOKEN_KEY, TOKEN_SECRET: TOKEN_SECRET, BUFFER_HOURS: BUFFER_HOURS, PAGE_SIZE: PAGE_SIZE };
                return configParams;
            }
            catch (err) {
                log.error({ title: 'customPreferences', details: err });
            }
        };

        this.isNotEmptyAndhasOwnProperty = function (obj, prop) {
            try {
                for (var prop1 in obj) {
                    if (obj.hasOwnProperty(prop))
                        return true;
                }
                return false;
            } catch (err) {
                return false;
            }
        };
        this.formatAmount = function (centAmount, fractionDigits) {
            try {
                //49900 2
                var x = centAmount.toString();//Integer.toString(centAmount);
                var f = Number(fractionDigits);//Integer.toString(fractionDigits);
                log.debug({ title: 'formatAmount', details: 'x:' + x + ' f:' + f });
                return x.substring(0, x.length - f) + "." + x.substring(x.length - f, x.length);
            } catch (err) {
                log.error({ title: 'formatAmount', details: err.message });
                return 0;
            }
        }
    };
});