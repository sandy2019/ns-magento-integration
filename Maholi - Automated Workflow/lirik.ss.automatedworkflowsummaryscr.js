/*******************************************************************
 *
 *
 * Name: lirik.ss.automatedworkflowsummaryscr.js
 * Script Type: Suitelet
 * @version: 1.0.1
 *
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 *
 *
 * Author: Lirik Inc.
 * Purpose: Summary Screen
 * Script: customscript_lirik_ss_autowrkflw_summscr
 * Deploy: customdeploy_lirik_ss_autowrkflw_summscr
 *
 *
 * ******************************************************************* */

define(['N/ui/serverWidget', './lib/lirik.screenConfig', './lib/lirik.utils', './lib/lodash_4.17.11_min'], suitelet);

function suitelet(serverWidget, scrConfig, utils, _) {


  function onRequest(params) {

    try {

      log.debug({ title: 'suitelet.onRequest', details: 'body ::' + params.request.body });

      var summaryHTML = '';

      if (params.request.body) {

        var body = JSON.parse(params.request.body);

        if (scrConfig.summary && Array.isArray(scrConfig.summary.groupBy) && scrConfig.summary.groupBy.length > 0) {

          var arrGroupBy = JSON.parse(JSON.stringify(scrConfig.summary.groupBy));

          summaryHTML = groupData({ groupBy: arrGroupBy, data: body });
        }

        params.response.setHeader({ name: 'Content-Type', value: 'text/plain' });
        params.response.write({ output: summaryHTML });
      }
    } catch (err) {
      log.error({ title: 'suitelet.onRequest', details: err });
      throw err;
    }
  }

  function groupData(params) {
    try {

      utils.debug('suitelet.groupData', { params: params });

      var html = '';

      var groupedHTML = '';
      var groupHeaderHTML = '';
      var contentHTML = '';

      var colorCss = '';

      var index;

      if (Array.isArray(params.groupBy) && params.groupBy.length > 0 && Array.isArray(params.data) && params.data.length > 0) {

        var groupedData = _.groupBy(params.data, params.groupBy[0].property);

        var groupedDataKeys = Object.keys(groupedData);
        utils.debug('suitelet.groupData', { groupedDataKeys: groupedDataKeys });

        if (params.groupBy.length > 1) {

          var subGroup = params.groupBy.splice(1, params.groupBy.length);

          for (index = 0; index < groupedDataKeys.length; index++) {

            contentHTML = groupData({ groupBy: subGroup, data: groupedData[groupedDataKeys[index]] });

            if (params.groupBy[0].secondHeader) {
              groupHeaderHTML = '<h3 class = "ui-widget-content-data_summary" style="float:left; width:100%' + colorCss + '" >' + '<ul style="float:left; width:100%" >' + '<li style="float:left;" >' + groupedDataKeys[index] + '</li>' + '<li style="float: right;" >' + scrConfig.summary.columns[params.groupBy[0].secondHeader.property].label + ' : ' + _.sumBy(groupedData[groupedDataKeys[index]], params.groupBy[0].secondHeader.property) + '</li>' + '</ul>' + '</h3>';
            } else {
              groupHeaderHTML = '<h3 class = "ui-widget-content-data_summary" style="float:left; width:100%' + colorCss + '" >' + '<ul style="float:left; width:100%" >' + '<li style="float:left;" >' + groupedDataKeys[index] + '</li>' + '</ul>' + '</h3>';
            }

            groupedHTML += '<div id="' + scrConfig.summary.columns[params.groupBy[0].property].id + '_accordion" style="font-size: 13px;" >' + '<div class="group" style="float:left; width:100%" >' + groupHeaderHTML + contentHTML + '</div>' + '</div>';
          }

          html += groupedHTML;

          utils.debug('suitelet.groupData', html);

          return html;
        } else if (params.groupBy.length === 1) {

          for (index = 0; index < groupedDataKeys.length; index++) {

            contentHTML = getContent({ data: groupedData[groupedDataKeys[index]] });

            if (params.groupBy[0].secondHeader) {

              groupHeaderHTML = '<h3 class = "ui-widget-content-data_summary" style="float:left; width:100%' + colorCss + '" >' + '<ul style="float:left; width:100%" >' + '<li style="float:left;" >' + groupedDataKeys[index] + '</li>' + '<li style="float: right;" >' + scrConfig.summary.columns[params.groupBy[0].secondHeader.property].label + ' : ' + _.sumBy(groupedData[groupedDataKeys[index]], params.groupBy[0].secondHeader.property) + '</li>' + '</ul>' + '</h3>';
            } else {

              groupHeaderHTML = '<h3 class = "ui-widget-content-data_summary" style="float:left; width:100%' + colorCss + '" >' + '<ul style="float:left; width:100%" >' + '<li style="float:left;" >' + groupedDataKeys[index] + '</li>' + '</ul>' + '</h3>';
            }

            groupedHTML += '<div id="' + scrConfig.summary.columns[params.groupBy[0].property].id + '_accordion" style="font-size: 13px;" >' + '<div class="group" style="float:left; width:100%" >' + groupHeaderHTML + contentHTML + '</div>' + '</div>';

          }

          html = '<div>' + groupedHTML + '</div>';

          utils.debug('suitelet.groupData ** Last **', html);

          return html;
        }
      }

      return html;
    } catch (err) {
      log.error({ title: 'suitelet.groupData', details: err });
      throw err;
    }
  }

  function getContent(params) {
    try {

      utils.debug('suitelet.getContent', { params: params });

      var contentHTML = '';

      if (_.isPlainObject(scrConfig.summary.columns) && Array.isArray(params.data) && params.data.length > 0) {

        var columnIndex;

        var arrColumnsKeys = Object.keys(params.data[0]);

        // Table Header
        for (columnIndex = 0; columnIndex < arrColumnsKeys.length; columnIndex++) {

          if (scrConfig.summary.columns[arrColumnsKeys[columnIndex]]) {

            if (scrConfig.summary.columns[arrColumnsKeys[columnIndex]].displayType !== serverWidget.FieldDisplayType.HIDDEN) {

              if (scrConfig.summary.columns[arrColumnsKeys[columnIndex]].width) {

                contentHTML += '<td style="font-weight: bolder; width:' + scrConfig.summary.columns[arrColumnsKeys[columnIndex]].width + '" >' +
                  scrConfig.summary.columns[arrColumnsKeys[columnIndex]].label + '</td>';
              } else {

                contentHTML += '<td style="font-weight: bolder;" >' +
                  scrConfig.summary.columns[arrColumnsKeys[columnIndex]].label + '</td>';
              }
            }
          }
        }
        contentHTML = '<tr style="font-weight: bolder;" >' + contentHTML + '</tr>';

        // Table Data
        for (var index = 0; index < params.data.length; index++) {

          contentHTML += '<tr>';
          for (columnIndex = 0; columnIndex < arrColumnsKeys.length; columnIndex++) {

            if (scrConfig.summary.columns[arrColumnsKeys[columnIndex]]) {

              if (scrConfig.summary.columns[arrColumnsKeys[columnIndex]].displayType !== serverWidget.FieldDisplayType.HIDDEN) {

                if (scrConfig.summary.columns[arrColumnsKeys[columnIndex]].width) {

                  contentHTML += '<td style="width:' + scrConfig.summary.columns[arrColumnsKeys[columnIndex]].width + '" >' + params.data[index][arrColumnsKeys[columnIndex]] + '</td>';
                } else {

                  contentHTML += '<td>' + params.data[index][arrColumnsKeys[columnIndex]] + '</td>';
                }
              }
            }
          }
          contentHTML += '</tr>';
        }

        contentHTML = '<div>' + '<table data-role="table" class="ui-responsive" border = "1" cellspacing="0" cellpadding="10" style="text-align:center" >' + '<tbody>' + contentHTML + '</tbody>' + '</table>' + '</div>';
      }

      utils.debug('suitelet.getContent', contentHTML);

      return contentHTML;
    } catch (err) {
      log.error({ title: 'suitelet.getContent', details: err });
      throw err;
    }
  }

  return {
    onRequest: onRequest
  };
}
