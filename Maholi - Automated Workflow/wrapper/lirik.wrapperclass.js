/*******************************************************************
 *
 *
 * Name: lirik.wrapperclass.js
 * Script Type: Library
 * @version: 5.3.0
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 *
 * Author: Lirik Inc.
 * Purpose: This script is used for warpper class over the screen.
 *
 *
 * ******************************************************************* */

define(['N/http', 'N/record', 'N/runtime', 'N/ui/serverWidget', 'N/search', '../lib/lirik.screenConfig', '../lib/lirik.utils', '../lib/lodash_4.17.11_min'], wrapper);

function wrapper(http, record, runtime, serverWidget, search, scrConfig, utils, _) {

  /**
   * This method called before generating the form.
   * @param {Object} params 
   * @param {Object} params.request 
   * @param {Object} params.response
   * @returns {boolean} return true if form is need to be generated, else false.
   */
  function preProcessing(params) {

    try {

      log.debug({ title: 'wrapper.preProcessing', details: 'params.request ::' + params.request });

      if (params.request && params.request.parameters.form_fld_action === 'SUBMIT') {

        var arrSelectedRecords = params.request.parameters.form_fld_selected_records;
        utils.debug('wrapper.preProcessing', 'arrSelectedRecords ::' + arrSelectedRecords);

        var data = {
          user: runtime.getCurrentUser().id,
          packDiffItemSeperately: params.request.parameters.form_fld_pck_diff_item_sep === 'T' ? true : false,
          packEveryItemSeperately: params.request.parameters.form_fld_pck_item_sep === 'T' ? true : false,
          shipDate: params.request.parameters.form_fld_ship_date,
          selectedRecords: JSON.parse(arrSelectedRecords)
        };

        var scriptParams = { custscript_aw_selected_records: data };
        utils.debug('wrapper.preProcessing', { scriptParams: scriptParams });

        utils.inititateScript({
          scriptId: scrConfig.onSubmitProcess.scriptId,
          deploymentId: scrConfig.onSubmitProcess.deploymentId,
          params: scriptParams
        });

        var statusData = {};
        statusData.custpage_script_id = scrConfig.onSubmitProcess.scriptId;
        statusData.custpage_script_deployment_id = scrConfig.onSubmitProcess.deploymentId;
        statusData.custpage_script_process_name = 'Automated Workflow - Progress';
        statusData.custpage_record_name = 'Your Orders';

        // statusData.custpage_back_button_label = 'Back to Process';
        // statusData.custpage_back_script_id = 'customscript_lirik_ss_autoworkflowscr';
        // statusData.custpage_back_script_deployment_id = 'customdeploy_lirik_ss_autoworkflowscr';

        params.response.sendRedirect({ type: http.RedirectType.SUITELET, identifier: scrConfig.onSubmitRedirect.scriptId, id: scrConfig.onSubmitRedirect.deploymentId, parameters: statusData });

        return false;
      }

      return true;
    } catch (err) {
      log.error({ title: 'wrapper.preProcessing', details: err });
      throw err;
    }
  }

  function addSublist(params) {

    try {

      log.debug({ title: 'wrapper.addSublist', details: 'params.nsSublist ::' + params.nsSublist });

      if (params.nsSublist && params.sublist) { }

      return params.nsSublist;
    } catch (err) {
      log.error({ title: 'wrapper.addSublist', details: err });
      throw err;
    }
  }

  function addSublistData(params) {

    try {

      log.debug({ title: 'wrapper.addSublistData', details: 'params.nsSublist ::' + params.nsSublist });

      if (params.nsForm && params.nsSublist && params.sublist && params.searchResults && params.searchResults.count > 0) {

        var nsFieldSelRecords = params.nsForm.getField({ id: 'form_sublist_fld_sel_rec_count' });
        if (nsFieldSelRecords) {
          nsFieldSelRecords.defaultValue = params.request.parameters.form_fld_selected_records ? JSON.parse(params.request.parameters.form_fld_selected_records).length.toString() : '0';
        }

        var nsFieldTotalRecords = params.nsForm.getField({ id: 'form_sublist_fld_total_rec_count' });
        if (nsFieldTotalRecords) {
          nsFieldTotalRecords.defaultValue = params.searchResults.count.toString();
        }
      }

      return true;
    } catch (err) {
      log.error({ title: 'wrapper.addSublistData', details: err });
      throw err;
    }
  }

  function setSublistValue(params) {

    try {

      log.debug({ title: 'wrapper.setSublistValue', details: '' });

      if (params.request) { }

    } catch (err) {
      log.error({ title: 'wrapper.setSublistValue', details: err });
      throw err;
    }
  }

  return {
    addSublist: addSublist,
    addSublistData: addSublistData,
    setSublistValue: setSublistValue,
    preProcessing: preProcessing
  };
}
