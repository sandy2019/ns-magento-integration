/*******************************************************************
 *
 *
 * Name: lirik.screenConfig.js
 * Script Type: library
 * @version: 5.7.1
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 *
 * Author: Lirik Inc.
 * Purpose: This file contains all the constants & some record and field mapping
 * Script:
 * Deploy:
 *
 *
 * ******************************************************************* */

define(['N/ui/serverWidget', 'N/format', 'N/search'], function (serverWidget, format, search) {

  return {
    clientScriptModulePath: './lirik.cs.automatedworkflowscreen.js',
    form: {
      title: 'Automated Workflow',
      hideNavBar: false
    },
    buttons: [
      { id: 'form_btn_reset', label: 'Reset', functionName: 'customReset' },
      { id: 'form_btn_search', label: 'Search', functionName: 'customSearch' },
      { id: 'form_btn_submit', label: 'Submit', functionName: 'customSubmit', isSubmit: true }
    ],
    fieldsGroups: [
      { id: 'form_fldgrp_general_filters', label: 'General Filters', tab: undefined },
      { id: 'form_fldgrp_fields', label: 'Select Fields' }
    ],
    filterFields: [
      { id: 'form_fld_customer', label: 'Customer', type: serverWidget.FieldType.SELECT, source: 'customer', container: 'form_fldgrp_general_filters', defaultValue: '' },
      { id: 'form_fld_po', label: '#PO', type: serverWidget.FieldType.TEXT, container: 'form_fldgrp_general_filters', retainValue: true },
      { id: 'form_fld_so', label: '#SO', type: serverWidget.FieldType.TEXT, container: 'form_fldgrp_general_filters', retainValue: true }
    ],
    fields: [
      { id: 'form_fld_ship_date', label: 'Ship Date', type: serverWidget.FieldType.DATE, container: 'form_fldgrp_fields', defaultValue: format.format({ value: new Date(), type: format.Type.DATE }), retainValue: true },
      { id: 'form_fld_pck_diff_item_sep', label: 'Pack Different Item Separately', type: serverWidget.FieldType.CHECKBOX, container: 'form_fldgrp_fields', retainValue: true },
      { id: 'form_fld_pck_item_sep', label: 'Pack Every Item Separately', type: serverWidget.FieldType.CHECKBOX, container: 'form_fldgrp_fields', retainValue: true },
      { id: 'form_fld_shipping_method', label: 'Shipping Method', type: serverWidget.FieldType.SELECT, breakType: serverWidget.FieldBreakType.STARTCOL, source: '-192', container: 'form_fldgrp_fields' },
      { id: 'form_fld_item_package', label: 'Package', type: serverWidget.FieldType.SELECT, source: 'customrecord_sps_pack_type', container: 'form_fldgrp_fields' },
      { id: 'form_fld_item_status_code_1', label: 'Item Status Code 1', type: serverWidget.FieldType.SELECT, breakType: serverWidget.FieldBreakType.STARTCOL, source: 'customlist34', container: 'form_fldgrp_fields' },
      { id: 'form_fld_item_schedule_qualifier_1', label: 'Item Schedule Qualifier 1', type: serverWidget.FieldType.SELECT, source: 'customlist_sps_datequalifiers_m', container: 'form_fldgrp_fields' },
      { id: 'form_fld_item_schedule_date_1', label: 'Item Schedule Date 1', type: serverWidget.FieldType.DATE, container: 'form_fldgrp_fields' },
      { id: 'form_fld_item_acknowledgement_type', label: 'Acknowledgment Type', type: serverWidget.FieldType.SELECT, source: 'customlist_sps_ack_header_types_m', container: 'form_fldgrp_fields' },
      { id: 'form_sublist_fld_sel_rec_count', label: 'Selected Records', type: serverWidget.FieldType.TEXT, displayType: serverWidget.FieldDisplayType.INLINE, container: 'form_subtab_so_items', defaultValue: '0', retainValue: true },
      { id: 'form_sublist_fld_total_rec_count', label: 'Total Records', type: serverWidget.FieldType.TEXT, displayType: serverWidget.FieldDisplayType.INLINE, breakType: serverWidget.FieldBreakType.STARTCOL, container: 'form_subtab_so_items', defaultValue: '0' },
      { id: 'form_fld_selected_records', label: 'Selected Records', type: serverWidget.FieldType.LONGTEXT, displayType: serverWidget.FieldDisplayType.HIDDEN, defaultValue: '[]', retainValue: true },
      { id: 'form_fld_action', label: 'Action', type: serverWidget.FieldType.TEXT, displayType: serverWidget.FieldDisplayType.HIDDEN, defaultValue: '[]', retainValue: true }
    ],
    subtabs: [{ id: 'form_subtab_so_items', label: 'Sales Oders Items', tab: undefined }],
    sublists: [{
      id: 'form_sublist_so_items',
      label: 'Sales Oders Items',
      type: serverWidget.SublistType.LIST,
      tab: 'form_subtab_so_items',
      selectedRecordsField: 'form_fld_selected_records',
      findFields: {
        id: 'internalid',
        lineUniqueKey: 'lineuniquekey'
      },
      selectField: 'form_sublist_col_select',
      columns: [
        { id: 'form_sublist_col_select', label: 'Select', type: serverWidget.FieldType.CHECKBOX },
        {
          id: 'form_sublist_col_internalid',
          label: 'Internal Id',
          type: serverWidget.FieldType.TEXT,
          displayType: serverWidget.FieldDisplayType.HIDDEN,
          width: 3,
          search: { name: 'internalid' }
        },
        {
          id: 'form_sublist_col_trandate',
          label: 'Date',
          type: serverWidget.FieldType.TEXT,
          search: { name: 'trandate', sort: search.Sort.ASC }
        },
        {
          id: 'form_sublist_col_po',
          label: '#PO',
          type: serverWidget.FieldType.TEXT,
          search: { name: 'otherrefnum', sort: search.Sort.ASC }
        },
        {
          id: 'form_sublist_col_lineuniquekey',
          label: 'Line Unique Key',
          type: serverWidget.FieldType.TEXT,
          displayType: serverWidget.FieldDisplayType.HIDDEN,
          width: 3,
          search: { name: 'lineuniquekey', sort: search.Sort.ASC }
        },
        {
          id: 'form_sublist_col_customer',
          label: 'Customer',
          type: serverWidget.FieldType.TEXT,
          displayType: serverWidget.FieldDisplayType.HIDDEN,
          search: { name: 'entity' }
        },
        {
          id: 'form_sublist_col_customer_name',
          label: 'Name',
          type: serverWidget.FieldType.TEXT,
          search: { name: 'entity', isText: true }
        },
        {
          id: 'form_sublist_col_item',
          label: 'Item',
          type: serverWidget.FieldType.TEXT,
          displayType: serverWidget.FieldDisplayType.HIDDEN,
          height: 3,
          width: 30,
          search: { name: 'item' }
        },
        {
          id: 'form_sublist_col_item_name',
          label: 'Item Name',
          type: serverWidget.FieldType.TEXT,
          height: 3,
          width: 30,
          search: { name: 'item', isText: true }
        },
        {
          id: 'form_sublist_col_item_desc',
          label: 'Item Desc',
          type: serverWidget.FieldType.TEXT,
          height: 3,
          width: 30,
          search: { name: 'salesdescription', join: 'item' }
        },
        {
          id: 'form_sublist_col_lineitem_shipping_method',
          label: 'Shipping Method',
          type: serverWidget.FieldType.SELECT,
          requestProperty: 'shipMethod',
          source: '-192',
          search: { defaultValue: '6938' }
        },
        {
          id: 'form_sublist_col_lineitem_package',
          label: 'Package',
          type: serverWidget.FieldType.SELECT,
          requestProperty: 'pckId',
          source: 'customrecord_sps_pack_type',
          search: { defaultValue: '3' }
        },
        {
          id: 'form_sublist_col_lineitem_trackno',
          label: 'Tracking No.',
          type: serverWidget.FieldType.TEXT,
          displayType: serverWidget.FieldDisplayType.ENTRY,
          requestProperty: 'trackno'
        },
        {
          id: 'form_sublist_col_lineitem_commited',
          label: 'Committed',
          type: serverWidget.FieldType.TEXT,
          width: 3,
          search: { name: 'quantitycommitted' }
        },
        {
          id: 'form_sublist_col_lineitem_fulfilled',
          label: 'Fulfilled',
          type: serverWidget.FieldType.TEXT,
          width: 3,
          search: { name: 'quantityshiprecv' }
        },
        {
          id: 'form_sublist_col_lineitem_invoiced',
          label: 'Invoiced',
          type: serverWidget.FieldType.TEXT,
          width: 3,
          search: { name: 'quantitybilled' }
        },
        {
          id: 'form_sublist_col_lineitem_quantity_ordered',
          label: 'Qty Ordered',
          type: serverWidget.FieldType.INTEGER,
          displayType: serverWidget.FieldDisplayType.TEXT,
          width: 3,
          search: { name: 'quantity' }
        },
        {
          id: 'form_sublist_col_lineitem_quantity',
          label: 'Qty',
          type: serverWidget.FieldType.INTEGER,
          displayType: serverWidget.FieldDisplayType.ENTRY,
          width: 3,
          requestProperty: 'qty',
          search: { name: 'quantity' }
        },
        {
          id: 'form_sublist_col_lineitem_rate',
          label: 'Rate',
          type: serverWidget.FieldType.CURRENCY,
          displayType: serverWidget.FieldDisplayType.ENTRY,
          width: 4,
          search: { name: 'rate' }
        },
        {
          id: 'form_sublist_col_lineitem_tp_price',
          label: 'TP Price',
          type: serverWidget.FieldType.CURRENCY,
          displayType: serverWidget.FieldDisplayType.ENTRY,
          width: 4,
          search: { name: 'custcol_sps_purchaseprice' }
        },
        {
          id: 'form_sublist_col_lineitem_amount',
          label: 'Amount',
          type: serverWidget.FieldType.CURRENCY,
          displayType: serverWidget.FieldDisplayType.ENTRY,
          width: 4,
          search: { name: 'amount' }
        },
        {
          id: 'form_sublist_col_lineitem_status_code_1',
          label: 'Item Status Code 1',
          type: serverWidget.FieldType.SELECT,
          source: 'customlist34',
          requestProperty: 'spsStatusCodeId',
          search: { name: 'custcol_sps_itemstatuscode1', defaultValue: '1' }
        },
        {
          id: 'form_sublist_col_lineitem_schedule_qty_1',
          label: 'Item Schedule Qty 1',
          type: serverWidget.FieldType.INTEGER,
          displayType: serverWidget.FieldDisplayType.ENTRY,
          width: 3,
          requestProperty: 'spsItemScheduleQty',
          search: { name: 'quantity' }
        },
        {
          id: 'form_sublist_col_lineitem_schedule_qualifier_1',
          label: 'Item Schedule Qualifier 1',
          type: serverWidget.FieldType.SELECT,
          source: 'customlist_sps_datequalifiers_m',
          requestProperty: 'spsItemScheduleQualId',
          search: { name: 'custcol_sps_itemschedulequalifier1', defaultValue: '5' }
        },
        {
          id: 'form_sublist_col_lineitem_schedule_date_1',
          label: 'Item Schedule Date 1',
          type: serverWidget.FieldType.DATE,
          displayType: serverWidget.FieldDisplayType.ENTRY,
          width: 1,
          requestProperty: 'spsItemScheduleDt',
          search: { name: 'custcol_sps_itemscheduledate1', defaultValue: format.format({ value: new Date(), type: format.Type.DATE }) }
        },
        {
          id: 'form_sublist_col_lineitem_acknowledgement_type',
          label: 'Acknowledgment Type',
          type: serverWidget.FieldType.SELECT,
          source: 'customlist_sps_ack_header_types_m',
          requestProperty: 'spsAckTypeId',
          search: { name: 'custbody_sps_acknowledgement_type', defaultValue: '1' }
        },
        {
          id: 'form_sublist_col_po_last',
          label: '#PO',
          type: serverWidget.FieldType.TEXT,
          search: { name: 'otherrefnum' }
        }
      ],
      nsRecordSearchId: 'salesorder',
      filters: {
        default: [
          ['type', 'anyof', 'SalesOrd'],
          'AND',
          ['mainline', 'is', 'F'],
          'AND',
          ['shipping', 'is', 'F'],
          'AND',
          ['taxline', 'is', 'F'],
          'AND',
          ['memorized', 'is', 'F'],
          'AND',
          ['status', 'anyof', 'SalesOrd:B', 'SalesOrd:D', 'SalesOrd:E'],
          'AND',
          ['quantitycommitted', 'greaterthan', 0],
          'AND',
          ['quantityshiprecv', 'equalto', 0],
          'AND',
          ['quantitybilled', 'equalto', 0],
          'AND',
          ['item', 'noneof', '6938'],
          'AND',
          ['formulanumeric: CASE WHEN {quantitycommitted} = {quantity} THEN 1 ELSE 0 END', 'equalto', '1'],
          'AND',
          ['custbody_aw_error', 'isempty', '']
        ],
        request: [
          { id: 'form_fld_customer', searchField: 'name', searchOperator: search.Operator.ANYOF },
          { id: 'form_fld_po', searchField: 'otherrefnum', searchOperator: search.Operator.EQUALTO },
          { id: 'form_fld_so', searchField: 'tranid', searchOperator: search.Operator.STARTSWITH }
        ]
      },
      includeInactive: true,
      dataOnFirstLoad: true,
      dataLoadFilterField: 'form_fld_customer',
      buttons: [
        { id: 'form_sublist_btn_markall', label: 'Mark All', functionName: 'customMarkAll' },
        { id: 'form_sublist_btn_unmarkall', label: 'Unmark All', functionName: 'customUnmarkAll' }
      ],
      pagination: { id: 'form_sublist_fld_pagination', pageSize: 100, container: 'form_subtab_so_items' }
    }],
    onSubmitProcess: {
      scriptId: 'customscript_lirik_sc_processorders',
      deploymentId: 'customdeploy_lirik_sc_processorders',
    },
    onSubmitRedirect: {
      scriptId: 'customscript_ss_script_execution_status',
      deploymentId: 'customdeploy_ss_script_execution_status',
    }
  };
});