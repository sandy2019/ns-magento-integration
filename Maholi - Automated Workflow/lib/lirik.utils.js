/*******************************************************************
 *
 *
 * Name: lirik.utils.js
 * Script Type: library
 * @version: 1.1.0
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 *
 * Author: Lirik Inc.
 * Purpose: Library for all the utitity api
 *
 *
 * ******************************************************************* */


define(['N/runtime', 'N/task', 'N/search', './lodash_4.17.11_min'], utils);

function utils(runtime, task, search, _) {

  /**
   * Checks if the input string contains any integer.
   * @param {string} value 
   * @returns {boolean}
   */
  function checkInteger(value) {
    try {
      return /^[-]*\d+$/.test(value);
    } catch (err) {
      log.error('utils.checkInteger', err);
      throw err;
    }
  }

  /**
   * Checks the execution status of the scheduled script
   * @param {Object} params 
   * @param {string[]} params.scriptIds - Array of Script Ids.
   * @returns { { scriptId:string, status: string, percentComplete: string } } returns current Status and PercentComplete of the scripts.
   */
  function checkScriptStatus(params) {
    try {

      debug('utils.checkScriptStatus', { params: params });

      if (Array.isArray(params.scriptIds) && params.scriptIds.length > 0) {

        var seaFiltersScriptIds = []
        for (var index = 0; index < params.scriptIds.length; index++) {

          seaFiltersScriptIds.push(['script.scriptid', search.Operator.IS, params.scriptIds[index]]);
          seaFiltersScriptIds.push('OR');
        }
        if (seaFiltersScriptIds[seaFiltersScriptIds.length - 1] === 'OR') {
          seaFiltersScriptIds = seaFiltersScriptIds.slice(0, seaFiltersScriptIds.length - 1);
        }

        var seaFiltersStatus = [];
        seaFiltersStatus.push(['status', search.Operator.ANYOF, ['PENDING', 'PROCESSING']]);

        var seaFilters = [seaFiltersScriptIds, 'AND', seaFiltersStatus];
        debug('utils.checkScriptStatus', { seaFilters: seaFilters });

        var seaColumns = [];
        seaColumns.push(search.createColumn({ name: 'startdate', sort: search.Sort.DESC }));
        seaColumns.push(search.createColumn({ name: 'enddate' }));
        seaColumns.push(search.createColumn({ name: 'scriptid', join: 'script' }));
        seaColumns.push(search.createColumn({ name: 'status' }));
        seaColumns.push(search.createColumn({ name: 'percentcomplete' }));

        var nsSearch = search.create({
          type: search.Type.SCHEDULED_SCRIPT_INSTANCE, filters: seaFilters, columns: seaColumns
        });
        var nsSearchResultSet = nsSearch.run();
        var nsSearchResults = nsSearchResultSet.getRange({ start: 0, end: 5 });

        if (nsSearchResults && nsSearchResults.length > 0) {

          var script = {
            scriptId: nsSearchResults[0].getValue({ name: 'scriptid', join: 'script' }),
            status: nsSearchResults[0].getValue({ name: 'status' }),
            percentComplete: nsSearchResults[0].getValue({ name: 'percentcomplete' })
          };
          debug('utils.checkScriptStatus', { script: script });

          return script;
        }
      }
    } catch (err) {
      log.error('utils.checkScriptStatus', err);
      throw err;
    }
  }

  /**
   * Prints log on NetSuite SuiteScript
   * @param {string} title - Log title e.g. calling function name.
   * @param {*} details - Details that need to be logged.
   * @returns {void}
   */
  function debug(title, details) {
    try {
      if (typeof details === 'object') {
        log.debug(title, JSON.stringify(details));
      } else {
        log.debug(title, details);
      }
    } catch (err) {
      log.error('utils.log', err);
      throw err;
    }
  }

  /**
   * Formats the value based on desired type.
   * @param {Object} params
   * @param {Object} params.mapping
   * @param {Object} params.mapping.type - Type in which value needs to be formatted.
   * @param {string} params.value - String value which needs to be formatted.
   * @returns {*} formatted value.
   */
  function formatData(params) {
    try {
      /*
      debug('utils.formatData', {
        params: params
      });
      */
      if (params.mapping.type === 'boolean') {
        return params.value === 'T' ? true : false;
      } else if (params.mapping.type === 'currency') {
        return Number(params.value).toFixed(2);
      } else if (params.mapping.type === 'number') {
        return Number(params.value);
      } else {
        return params.value;
      }
    } catch (err) {
      log.error('utils.formatData', err);
      throw err;
    }
  }

  /**
   * Reads the data from NetSuite record and return the data in JSON format.
   * @param {Object} params
   * @param {Object} params.nsRecord - NetSuite record object.
   * @param {Object} [params.fields]
   * @param {Object} params.fields.nsId - NetSuite field id.
   * @param {Object} params.fields.type - Type in which value needs to be formatted.
   * @param {Object} params.fields.isText - Whether to get select field's option value.
   * @param {Object} [params.lineitems]
   * @param {Object} params.lineitems.name - Sublist name for returning sublist JSON .
   * @param {Object} params.lineitems.fields
   * @param {Object} params.lineitems.fields.nsId - NetSuite sublist field id.
   * @param {Object} params.lineitems.fields.type - Type in which value needs to be formatted.
   * @param {Object} params.lineitems.fields.isText - Whether to get select field's option value.
   * @returns {Object}
   */
  function getRecordFieldValues(params) {
    try {

      if (!params.recordObject) {
        params.recordObject = {};
      }

      for (var key in params.fieldMapping) {

        if (params.fieldMapping.hasOwnProperty(key) && key === 'fields') {

          params.recordObject.fields = {};
          var arrFieldKeys = Object.keys(params.fieldMapping.fields);
          for (var fieldKeyIndex = 0; fieldKeyIndex < arrFieldKeys.length; fieldKeyIndex++) {

            var fieldKey = arrFieldKeys[fieldKeyIndex];
            var fieldMap = params.fieldMapping.fields[fieldKey];

            if (fieldMap.isText) {
              params.recordObject.fields[fieldKey] = params.nsRecord.getText({
                'fi​e​l​d​I​d': fieldMap.nsId
              }) || null;
            } else {
              params.recordObject.fields[fieldKey] = params.nsRecord.getValue({
                'fi​e​l​d​I​d': fieldMap.nsId
              }) || null;
            }

            params.recordObject.fields[fieldKey] = formatData({
              'mapping': fieldMap,
              'value': params.recordObject.fields[fieldKey]
            });
          }
        } else if (params.fieldMapping.hasOwnProperty(key) && key === 'lineitems') {

          params.recordObject.lineitems = {};
          var arrLineItemKeys = Object.keys(params.fieldMapping.lineitems);

          for (var lineItemKeyIndex = 0; lineItemKeyIndex < arrLineItemKeys.length; lineItemKeyIndex++) {

            var lineItemKey = arrLineItemKeys[lineItemKeyIndex];
            var lineItemMap = params.fieldMapping.lineitems[lineItemKey];

            var lineItemName = lineItemKey;
            if (lineItemMap.name) {
              lineItemName = lineItemMap.name;
            }

            params.recordObject.lineitems[lineItemName] = [];

            var listItemLinesCount = params.nsRecord.getLineCount({
              'sublistId': lineItemKey
            });

            /*
            debug('utils.getRecordFieldValues', {
              params.recordObject: params.recordObject
            });
            debug('utils.getRecordFieldValues', {
              listItemLinesCount: listItemLinesCount
            });
            */

            for (var currline = 0; currline < listItemLinesCount; currline++) {
              params.nsRecord.selectLine({
                'sublistId': lineItemKey,
                'line': currline
              });

              var lineItemData = {};
              var arrLineItemFieldKeys = Object.keys(lineItemMap.fields);
              for (var lineItemFieldKeyIndex = 0; lineItemFieldKeyIndex < arrLineItemFieldKeys.length; lineItemFieldKeyIndex++) {

                var lineItemFieldKey = arrLineItemFieldKeys[lineItemFieldKeyIndex];
                var lineItemFieldMap = lineItemMap.fields[lineItemFieldKey];

                if (lineItemFieldMap.isText) {
                  lineItemData[lineItemFieldKey] = params.nsRecord.getCurrentSublistText({
                    'sublistId': lineItemKey,
                    'fieldId': lineItemFieldMap.nsId
                  }) || null;
                } else {
                  lineItemData[lineItemFieldKey] = params.nsRecord.getCurrentSublistValue({
                    'sublistId': lineItemKey,
                    'fieldId': lineItemFieldMap.nsId
                  }) || null;
                }

                lineItemData[lineItemFieldKey] = formatData({
                  'mapping': lineItemFieldMap,
                  'value': lineItemData[lineItemFieldKey]
                });
              }

              params.recordObject.lineitems[lineItemName].push(lineItemData);
            }
          }
        }
      }

      params.recordObject.recordId = params.nsRecord.id;
      params.recordObject.recordType = params.nsRecord.type;

      debug('utils.getRecordFieldValues', {
        recordObject: params.recordObject
      });

      return params.recordObject;
    } catch (err) {
      log.error('utils.getRecordFieldValues', err);
      throw err;
    }
  }

  /**
   * Provides the Script parameters' value
   * @param {Object} params
   * @returns {Object}
   */
  function getScriptParameters(params) {
    try {
      debug('utils.getScriptParameters', { params: params });
      var scrParamKeys = Object.keys(params);

      if (scrParamKeys && scrParamKeys.length > 0) {

        var scriptParams = {};

        var _nsScriptRec = runtime.getCurrentScript();

        for (var scrParamIndex = 0; scrParamIndex < scrParamKeys.length; scrParamIndex++) {

          scriptParams[scrParamKeys[scrParamIndex]] = _nsScriptRec.getParameter({
            name: params[scrParamKeys[scrParamIndex]]
          });
        }

        debug('utils.getScriptParameters', { scriptParams: scriptParams });

        return scriptParams;
      }
    } catch (err) {
      log.error('utils.getScriptParameters', err);
      throw err;
    }
  }

  /**
   * Returns the searched data in JSON format
   * @param {Object} searchResult 
   * @returns {Object}
   */
  function getSearchValues(searchResult) {
    try {
      if (searchResult) {
        var searchResultValues = { id: searchResult.id, recordType: searchResult.recordType };
        for (var columnIndex = 0; columnIndex < searchResult.columns.length; columnIndex++) {
          if (searchResult.columns[columnIndex].join) {
            searchResultValues[searchResult.columns[columnIndex].join + '.' + searchResult.columns[columnIndex].name] = searchResult.getValue(searchResult.columns[columnIndex]);
          } else {
            searchResultValues[searchResult.columns[columnIndex].name] = searchResult.getValue(searchResult.columns[columnIndex]);
          }
        }
        debug('utils.getSearchValues', { searchResultValues: searchResultValues });
        return searchResultValues;
      }
    } catch (err) {
      log.error('utils.getSearchValues', err);
      throw err;
    }
  }

  /**
   * Returns the searched data with text values in JSON format
   * @param {Object} searchResult
   * @returns {Object}
   */
  function getSearchValuesWithText(params) {
    try {
      var searchResultValues = {};
      for (var columnIndex = 0; columnIndex < params.columns.length; columnIndex++) {

        if (params.columns[columnIndex].join) {
          searchResultValues[params.columns[columnIndex].join + '.' + params.columns[columnIndex].name] = {
            value: params.searchResult.getValue(params.columns[columnIndex]),
            text: params.searchResult.getText(params.columns[columnIndex])
          };
        } else {
          searchResultValues[params.columns[columnIndex].name] = {
            value: params.searchResult.getValue(params.columns[columnIndex]),
            text: params.searchResult.getText(params.columns[columnIndex])
          };
        }
      }
      return searchResultValues;
    } catch (err) {
      log.error('utils.getSearchValuesWithText', err);
      throw err;
    }
  }

  /**
   * Starts a Scheduled or Map/Reduce script.
   * @param {Object} params
   * @param {string} params.type - MAP_REDUCE if the script is Map/Reduce else system will consider script as schedule script.
   * @param {string} params.scriptId - Script's script id.
   * @param {string} [params.deploymentId] - Script's deployment id.
   * @param {Object} [params.params] - Parameters for rescheduled script.
   * @returns {string} returns a unique ID for the task.
   */
  function inititateScript(params) {
    try {

      log.debug('inititateScript', { params: params });

      if (params.type === 'MAP_REDUCE') {
        scriptTask = task.create({ taskType: task.TaskType.MAP_REDUCE });
      } else {
        scriptTask = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT });
      }

      scriptTask.scriptId = params.scriptId;
      scriptTask.deploymentId = params.deploymentId ? params.deploymentId : null;
      scriptTask.params = params.params ? params.params : null;
      return scriptTask.submit();
    } catch (err) {
      log.error('utils.inititateScript', 'ERROR');
      throw err;
    }
  }

  /**
   * Set the fields value.
   * @param {Object} params
   * @param {Object} params.fields
   * @param {boolean} params.ignoreFieldChange - Whether field change event shall fire or not.
   * @returns {void}
   */
  function setFieldsValues(params) {
    try {
      debug('utils.setFieldsValues', { fields: params.fields, ignoreFieldChange: params.ignoreFieldChange });

      for (var key in params.fields) {
        params.nsRecord.setValue({
          fieldId: key,
          value: params.fields[key],
          ignoreFieldChange: params.ignoreFieldChange
        });
      }
    } catch (err) {
      log.error('utils.setFieldsValues', err);
      throw err;
    }
  }

  /**
   * Creates a new sublist line and sets the fields value.
   * @param {Object} params
   * @param {string} params.sublistId - Sublist id for which line needs to be selected.
   * @param {Object} params.fields
   * @param {boolean} params.ignoreFieldChange - Whether field change event shall fire or not.
   * @returns {void}
   */
  function selectLine(params) {
    try {
      debug('utils.selectLine', { fields: params.fields, ignoreFieldChange: params.ignoreFieldChange, sublistId: params.sublistId, line: params.line });

      if (params.sublistId && params.line) {
        params.nsRecord.selectLine({ sublistId: params.sublistId, line: params.line });

        for (var key in params.fields) {
          params.nsRecord.setCurrentSublistValue({
            sublistId: params.sublistId,
            fieldId: key,
            value: params.fields[key],
            ignoreFieldChange: params.ignoreFieldChange
          });
        }

        params.nsRecord.commitLine({ sublistId: params.sublistId });
      }
    } catch (err) {
      log.error('utils.selectLine', err);
      throw err;
    }
  }

  /**
   * Creates a new sublist line and sets the fields value.
   * @param {Object} params
   * @param {string} params.sublistId - Sublist id for which new line needs to be created.
   * @param {Object} params.fields
   * @param {boolean} params.ignoreFieldChange - Whether field change event shall fire or not.
   * @returns {void}
   */
  function selectNewLine(params) {
    try {
      debug('utils.selectNewLine', { fields: params.fields, ignoreFieldChange: params.ignoreFieldChange, sublistId: params.sublistId });

      if (params.sublistId) {
        params.nsRecord.selectNewLine({ sublistId: params.sublistId });

        for (var key in params.fields) {
          params.nsRecord.setCurrentSublistValue({
            sublistId: params.sublistId,
            fieldId: key,
            value: params.fields[key],
            ignoreFieldChange: params.ignoreFieldChange
          });
        }

        params.nsRecord.commitLine({ sublistId: params.sublistId });
      }
    } catch (err) {
      log.error('utils.selectNewLine', err);
      throw err;
    }
  }

  return {
    checkInteger: checkInteger,
    checkScriptStatus: checkScriptStatus,
    debug: debug,
    getRecordFieldValues: getRecordFieldValues,
    getScriptParameters: getScriptParameters,
    getSearchValues: getSearchValues,
    getSearchValuesWithText: getSearchValuesWithText,
    inititateScript: inititateScript,
    setFieldsValues: setFieldsValues,
    selectLine: selectLine,
    selectNewLine: selectNewLine
  };
}
