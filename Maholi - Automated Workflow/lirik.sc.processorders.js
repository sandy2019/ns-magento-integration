/*******************************************************************
 *
 *
 * Name: lirik.sc.processorders.js
 * Script Type: ScheduledScript
 * @version: 5.7.0
 *
 *
 * Author: Lirik Inc.
 * Purpose: This file is used to process bulk Sales Order.
 * Script: customscript_lirik_sc_processorders
 * Deploy: customdeploy_lirik_sc_processorders
 *
 *
 * ******************************************************************* */


 function start(type) {

  try {

    utils.debug('start', { type: type });

    var _nsContext = nlapiGetContext();

    var data = _nsContext.getSetting('SCRIPT', 'custscript_aw_selected_records');

    var fileFolderId = _nsContext.getSetting('SCRIPT', 'custscript_aw_folder_id');
    nlapiLogExecution('DEBUG', 'start', 'fileFolderId ::' + fileFolderId + '::');

    var emailFrom = _nsContext.getSetting('SCRIPT', 'custscript_aw_email_from');
    nlapiLogExecution('DEBUG', 'start', 'emailFrom ::' + emailFrom + '::');

    var emailCC = _nsContext.getSetting('SCRIPT', 'custscript_aw_email_cc');
    emailCC = emailCC.split(',');
    nlapiLogExecution('DEBUG', 'start', 'emailCC ::' + emailCC + '::');

    /**
     * ! Start, below lines are only for testing purpose.
     */
    /*
    data = ;
    data = JSON.stringify(data);
    */
    /**
     * ! END, above lines are only for testing purpose.
     */

    if (data) {

      utils.debug('start', 'data ::' + data);

      var soDetails;
      var arrOrdersStatus = [];
      var orderStatus;

      data = JSON.parse(data);

      var arrSelectedRecords = data.selectedRecords;
      utils.debug('start', { arrSelectedRecords: arrSelectedRecords });

      var salesOrders = _.groupBy(arrSelectedRecords, 'id');
      var arrOrdersIds = Object.keys(salesOrders);
      /*var arrOrdersIds = [];
      for(var temp = 0; temp < 36; temp++){
        arrOrdersIds.push("902145");
      } */    
      utils.debug('start', { arrOrdersIds: arrOrdersIds });

      for (var index = 0; index < arrOrdersIds.length; index++) {
        soDetails = nlapiLookupField('salesorder', salesOrders[arrOrdersIds[index]][0].id, ['tranid', 'otherrefnum']);

        orderStatus = {
          Id: salesOrders[arrOrdersIds[index]][0].id,
          SalesOrder: soDetails.tranid,
          PO: soDetails.otherrefnum,
          Status: '',
          ErrorCode: '',
          ErrorDetails: '',
          ShippingLabel: '',
          Link: ''
        };

        try {

          processOrders({
            packDiffItemSeperately: data.packDiffItemSeperately,
            packEveryItemSeperately: data.packEveryItemSeperately,
            shipDate: data.shipDate,
            orderDetails: salesOrders[arrOrdersIds[index]],
            orderStatus: orderStatus
          });

          orderStatus.Status = 'COMPLETED';
        } catch (errOrder) {
          nlapiLogExecution('ERROR', 'start - errOrder', errOrder);

          orderStatus.Status = 'FAILED';

          if (errOrder instanceof nlobjError) {

            orderStatus.ErrorCode = errOrder.getCode();
            orderStatus.ErrorDetails = errOrder.getDetails();

          } else {
            orderStatus.ErrorDetails = errOrder.toString();
          }

          if (!errOrder instanceof nlobjError || (errOrder instanceof nlobjError && errOrder.getCode() !== 'ITEMS_MISMATCH')) {
            nlapiSubmitField('salesorder', orderStatus.Id, 'custbody_aw_error', orderStatus.ErrorCode + ':' + orderStatus.ErrorDetails);
          }
        }

        if (orderStatus && Array.isArray(orderStatus.ShippingLabel) && orderStatus.ShippingLabel.length > 0) {
          var arrShippingLabels = JSON.parse(JSON.stringify(orderStatus.ShippingLabel));

          var tempOrderStatus;
          for (var labelIndex = 0; labelIndex < arrShippingLabels.length; labelIndex++) {

            tempOrderStatus = JSON.parse(JSON.stringify(orderStatus));
            tempOrderStatus.ShippingLabel = arrShippingLabels[labelIndex];
            tempOrderStatus.Link = '=HYPERLINK("' + arrShippingLabels[labelIndex] + '", "Label Link")';
            arrOrdersStatus.push(tempOrderStatus);
          }
        } else {
          arrOrdersStatus.push(orderStatus);
        }

        utils.rescheduleScript({ nsContext: _nsContext, limit: 500 });
      }

      var automatedWokflowStatus = Papa.unparse(arrOrdersStatus);
      var fileName = 'Automated Workflow Status - ' + nlapiDateToString(new Date(), 'datetime');

      var _nsFile = nlapiCreateFile(fileName + '.csv', 'CSV', automatedWokflowStatus);
      _nsFile.setFolder(fileFolderId);
      var _nsFileId = nlapiSubmitFile(_nsFile);
      utils.debug('start', { _nsFileId: _nsFileId });

      if (_nsFileId && emailFrom) {
        var mailBody = 'Hi<br> <br> Please find attached for the Automated Process Status<br> <br> Thank you';       
       nlapiLogExecution('DEBUG', 'mailBody 139', mailBody);
       nlapiSendEmail(emailFrom, data.user, fileName, mailBody, emailCC, null, null, _nsFile, true);
       //nlapiSendEmail(emailFrom, "bharat.pandey@lirik.io", fileName, mailBody, ["sandeep.kumar@lirik.io", "bharat.pandey@lirik.io"], null, null, _nsFile, true);
       nlapiLogExecution('DEBUG', 'mailBody 141', mailBody);
      }
      
    }
  } catch (err) {
    nlapiLogExecution('ERROR', 'start', err);
    throw err;
  }
}

/**
 * 
 * @param {Object} params 
 * @param {Object[]} params.orderDetails
 * @param {string} params.shipDate
 * @param {boolean} params.packDiffItemSeperately
 * @param {boolean} params.packEveryItemSeperately
 * @param {Object} params.orderStatus - Sales Order status details
 * @param {string} params.orderStatus.ErrorDetails
 * @returns {void}
 */
function processOrders(params) {
  try {

    utils.debug('processOrders', { params: params });

    var arrOrderItems = params.orderDetails;
    arrOrderItems = _.sortBy(arrOrderItems, function (o) {
      return Number(o.lineUniqueKey);
    });
    utils.debug('processOrders', { arrOrderItems: arrOrderItems });

    var _nsRecordId = arrOrderItems[0].id;
    var _nsRecordCustomerId = arrOrderItems[0].custId;

    _nsRecord = nlapiLoadRecord('salesorder', _nsRecordId, { recordmode: 'dynamic' });

    var sublistId = 'item';
    var lineCount = _nsRecord.getLineItemCount(sublistId);
    if (lineCount !== arrOrderItems.length) {
      throw nlapiCreateError('ITEMS_MISMATCH', 'Some items are not selected for the Sales Order on Automated Workflow screen', true);
    }

    var isSPS = _nsRecord.getFieldValue('custbody_sps_routingkey').trim() ? true : false;

    /**
     * ! Start, Sales Order fields update
     */
    var fields = {
      shipdate: params.shipDate ? params.shipDate : undefined
    };
    if (isSPS) {
      fields.shipmethod = arrOrderItems[0].shipMethod ? arrOrderItems[0].shipMethod : undefined;
      fields.custbody_sps_acknowledgement_type = arrOrderItems[0].spsAckTypeId ? arrOrderItems[0].spsAckTypeId : undefined;
    }
    utils.setFieldsValues({ nsRecord: _nsRecord, fields: fields });

    var lineUniqueKey;
    var foundLineItem;
    var lineItemFields;
    for (var line = 1; line <= lineCount; line++) {

      lineUniqueKey = _nsRecord.getLineItemValue(sublistId, 'lineuniquekey', line);
      utils.debug('processOrders', { lineUniqueKey: lineUniqueKey });

      foundLineItem = _.find(arrOrderItems, { lineUniqueKey: lineUniqueKey });
      if (foundLineItem) {

        lineItemFields = {
          description: foundLineItem.desc,
        };
        if (isSPS) {
          lineItemFields.custcol_sps_itemstatuscode1 = foundLineItem.spsStatusCodeId;
          lineItemFields.custcol_sps_itemscheduleqty1 = foundLineItem.spsItemScheduleQty;
          lineItemFields.custcol_sps_itemschedulequalifier1 = foundLineItem.spsItemScheduleQualId;
          lineItemFields.custcol_sps_itemscheduledate1 = foundLineItem.spsItemScheduleDt;
        }
        utils.selectLine({ nsRecord: _nsRecord, sublistId: sublistId, line: line, lineItemFields: lineItemFields });

        foundLineItem.line = line;
        var itemDetails = nlapiLookupField('item', foundLineItem.item, ['weight']);
        foundLineItem.weight = itemDetails.weight;
      }
    }

    nlapiSubmitRecord(_nsRecord, true, false);
	 _nsRecord = null;
    /**
     * ! END, Sales Order fields update
     */

    /**
     * ! SPS API call for PO Acknowledgement on Sales Order.
     */
    var customerDetails = nlapiLookupField('customer', _nsRecordCustomerId, ['custentity_maholi_po_ack', 'custentity_maholi_asn', 'custentity_maholi_integration_status', 'custentity_maholi_easypost']);
    customerDetails.custentity_maholi_po_ack = customerDetails.custentity_maholi_po_ack === 'T' ? true : false;
    customerDetails.custentity_maholi_asn = customerDetails.custentity_maholi_asn === 'T' ? true : false;
    customerDetails.custentity_maholi_integration_status = customerDetails.custentity_maholi_integration_status === 'T' ? true : false;
    customerDetails.custentity_maholi_easypost = customerDetails.custentity_maholi_easypost === 'T' ? true : false;
    utils.debug('processOrders', { customerDetails: customerDetails });

    if (_nsRecordId && isSPS && customerDetails.custentity_maholi_po_ack) {
      var spsResult = spsapiCreatePOAck({ stId: [_nsRecordId] });
      utils.debug('processOrders', { spsResult: spsResult });
    }

    processItemFulfillment({
      id: _nsRecordId,
      items: arrOrderItems,
      packEveryItemSeperately: params.packEveryItemSeperately,
      packDiffItemSeperately: params.packDiffItemSeperately,
      customerDetails: customerDetails,
      isSPS: isSPS,
      orderStatus: params.orderStatus
    });

    if (customerDetails.custentity_maholi_integration_status && !(params.orderStatus && params.orderStatus.ErrorDetails)) {
      processInvoice({ id: _nsRecordId, isSPS: isSPS });
    } else if (!(params.orderStatus && params.orderStatus.ErrorDetails)) {
      processInvoice({ id: _nsRecordId, isSPS: false });
    }

    var _nsContext = nlapiGetContext();
    utils.rescheduleScript({ nsContext: _nsContext, limit: 500 });

  } catch (err) {
    nlapiLogExecution('ERROR', 'processOrders', err);
    throw err;
  }
}

/**
 * Transforms Sales Order to Item Fulfillment, Creates and Buys Shipments through Easypost & Creates SPS ASN file for transformed Item Fulfillment.
 * @param {Object} params
 * @param {string} params.id
 * @param {boolean} params.packDiffItemSeperately
 * @param {boolean} params.packEveryItemSeperately
 * @param {Object[]} params.items
 * @param {string} params.customerDetails
 * @param {boolean} params.isSPS
 * @param {Object} params.orderStatus - Sales Order status details
 * @param {string} params.orderStatus.ErrorDetails
 * @returns {void}
 */
function processItemFulfillment(params) {
  try {

    utils.debug('processItemFulfillment', { params: params });

    if (params && params.id) {

      var _nsRecordId;
      var _nsRecord = nlapiTransformRecord('salesorder', params.id, 'itemfulfillment');

      if (_nsRecord) {

        var fields = {
          shipstatus: 'C'
        };

        utils.setFieldsValues({ nsRecord: _nsRecord, fields: fields });

        _nsRecordId = nlapiSubmitRecord(_nsRecord, true, false);
        utils.debug('processItemFulfillment', { _nsRecordId: _nsRecordId });

        var index;
        if (params.packEveryItemSeperately) {

          for (index = 0; index < params.items.length; index++) {

            var itemQuantity = Number(params.items[index].qty);

            for (var quantity = 0; quantity < itemQuantity; quantity++) {

              var item = JSON.parse(JSON.stringify(params.items[index]));
              item.qty = '1';

              createSPSPackage({ itemFulfillment: _nsRecordId, items: [item] });
            }
          }

        } else if (params.packDiffItemSeperately) {

          var groupedItems = _.groupBy(params.items, function (o) {
            return o.id + '_' + o.lineUniqueKey;
          });

          var arrGroupedItemKeys = Object.keys(groupedItems);
          for (index = 0; index < arrGroupedItemKeys.length; index++) {
            createSPSPackage({ itemFulfillment: _nsRecordId, items: groupedItems[arrGroupedItemKeys[index]] });
          }

        } else {
          createSPSPackage({ itemFulfillment: _nsRecordId, items: params.items });
        }

        if (params.customerDetails && params.customerDetails.custentity_maholi_easypost) {
          startAction(null, null, null, _nsRecordId, params.orderStatus);
        }

        /**
         * ! SPS API call for ASN on Item Fulfillment.
         */
        if (params.isSPS && _nsRecordId && params.customerDetails && params.customerDetails.custentity_maholi_asn) {
          var spsResult = spsapiCreateASN({ stId: [_nsRecordId] });
          utils.debug('processItemFulfillment', { spsResult: spsResult });
        }
      }
	  
	   _nsRecord = null;
    }
  } catch (err) {
    nlapiLogExecution('ERROR', 'processItemFulfillment', err);
    throw err;
  }
}

/**
 * Creates SPS Package custom record.
 * @param {Object} params
 * @param {string} params.itemFulfillment
 * @param {Object[]} params.items
 * @returns {void}
 */
function createSPSPackage(params) {

  try {

    utils.debug('createSPSPackage', { params: params });

    var _nsRecord = nlapiCreateRecord('customrecord_sps_package', { recordmode: 'dynamic' });

    var totalQuantity = 0;
    var totalWeight = 0;
    _.map(params.items, function (o) {
      totalQuantity += Number(o.qty);
      totalWeight += Number(o.qty) * Number(o.weight);
    });

    var foundTrackingNoItem = _.find(params.items, function (o) {
      return o.trackno ? true : false;
    });

    var fields = {
      custrecord_sps_pack_asn: params.itemFulfillment,
      custrecord_sps_package_box_type: params.items[0].pckId,
      custrecord_sps_package_qty: totalQuantity,
      custrecord_sps_pk_weight: totalWeight,
    };
    if (foundTrackingNoItem) {
      fields.custrecord_sps_track_num = foundTrackingNoItem.trackno;
    }

    utils.setFieldsValues({ nsRecord: _nsRecord, fields: fields });
    var _nsRecordId = nlapiSubmitRecord(_nsRecord, true, false);

    for (var index = 0; index < params.items.length; index++) {

      var currItem = params.items[index];

      createSPSPackageContent({ spsPackage: _nsRecordId, itemFulfillment: params.itemFulfillment, item: currItem.item, line: currItem.line, quantity: currItem.qty, totalQuantity: totalQuantity, totalWeight: totalWeight });
    }
	
	 _nsRecord = null;
  } catch (err) {
    nlapiLogExecution('ERROR', 'createSPSPackage', err);
    throw err;
  }
}

/**
 * 
 * @param {Object} params 
 * @param {string} params.spsPackage
 * @param {string} params.itemFulfillment
 * @param {string} params.item
 * @param {string} params.line
 * @param {string} params.quantity
 * @param {number} params.totalQuantity
 * @param {number} params.totalWeight
 * @returns {void}
 */
function createSPSPackageContent(params) {

  try {

    utils.debug('createSPSPackageContent', { params: params });

    var _nsRecord = nlapiCreateRecord('customrecord_sps_content', { recordmode: 'dynamic' });

    var fields = {
      custrecord_sps_content_package: params.spsPackage,
      custrecord_pack_content_fulfillment: params.itemFulfillment,
      custrecord_sps_content_item: params.item,
      custrecord_sps_content_item_line_num: params.line,
      custrecord_sps_content_qty: params.quantity,
      custrecord_parent_pack_total_qty: params.totalQuantity,
      custrecord_parent_pack_total_weight: params.totalWeight
    };

    utils.setFieldsValues({ nsRecord: _nsRecord, fields: fields });
    var _nsRecordId = nlapiSubmitRecord(_nsRecord, true, false);
	
	 _nsRecord = null;

    var _nsContext = nlapiGetContext();
    utils.rescheduleScript({ nsContext: _nsContext, limit: 500 });
  } catch (err) {
    nlapiLogExecution('ERROR', 'createSPSPackage', err);
    throw err;
  }
}

/**
 * Transform Sales Order to Invoice and sets SPS Integration field to "Ready".
 * @param {Object} params 
 * @param {string} params.id
 * @param {boolean} param.isSPS
 * @returns {void}
 */
function processInvoice(params) {
  try {

    utils.debug('processInvoice', { params: params });

    if (params && params.id) {

      var _nsRecord = nlapiTransformRecord('salesorder', params.id, 'invoice');

      if (_nsRecord) {

        var fields = {
        };
        if (params.isSPS) {
          fields.custbodyintegrationstatus = '1'
        }

        utils.setFieldsValues({ nsRecord: _nsRecord, fields: fields });

        var _nsRecordId = nlapiSubmitRecord(_nsRecord, true, false);
        utils.debug('processInvoice', { _nsRecordId: _nsRecordId });
      }
	  
	   _nsRecord = null;
    }
  } catch (err) {
    nlapiLogExecution('ERROR', 'processInvoice', err);
    throw err;
  }
}
