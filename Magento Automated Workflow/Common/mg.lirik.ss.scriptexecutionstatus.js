/*******************************************************************
 *
 *
 * Name: lirik.ss.scriptexecutionstatus.js
 * Script Type: SuiteletScript
 * @version 1.0.0
 *
 * Author: Lirik Inc.
 * Purpose: This script will provide an interface to check the status of execution of the script.
 * Script: customscript_ss_script_execution_status,customscript_cs_script_execution_status
 * Deploy: customdeploy_ss_script_execution_status
 *
 *
 * ******************************************************************* */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
  try {

    var strImageName = "lirik_processing.gif";
    // Getting request parameters
    var strProcessName = request
      .getParameter("custpage_script_process_name");

    if (!strProcessName) {
      strProcessName = "Process Execution Status";
    }

    var strRecordName = request.getParameter("custpage_record_name");

    if (!strRecordName) {
      strRecordName = "Records";
    }

    var strScriptID = request.getParameter("custpage_script_id");
    var strScriptDeploymentID = request
      .getParameter("custpage_script_deployment_id");

    var strBackButtonLabel = request
      .getParameter("custpage_back_button_label");
    if (!strBackButtonLabel) {
      strBackButtonLabel = "Back";
    }
    var strBackScriptID = request.getParameter("custpage_back_script_id");
    var strBackScriptDeploymentID = request
      .getParameter("custpage_back_script_deployment_id");

    nlapiLogExecution("DEBUG", "suitelet", "strProcessName: " +
      strProcessName);
    nlapiLogExecution("DEBUG", "suitelet", "strRecordName: " +
      strRecordName);
    nlapiLogExecution("DEBUG", "suitelet", "strScriptID: " + strScriptID);
    nlapiLogExecution("DEBUG", "suitelet", "strScriptDeploymentID: " +
      strScriptDeploymentID);

    nlapiLogExecution("DEBUG", "suitelet", "strBackButtonLabel: " +
      strBackButtonLabel);
    nlapiLogExecution("DEBUG", "suitelet", "strBackScriptID: " +
      strBackScriptID);
    nlapiLogExecution("DEBUG", "suitelet", "strBackScriptDeploymentID: " +
      strBackScriptDeploymentID);

    var objForm = nlapiCreateForm(strProcessName, false);
    // Set the client script on form
    objForm.setScript("customscript_cs_script_execution_status");

    // Adding fields to form object
    objForm.addFieldGroup("custpage_header_group", "Header", null)
      .setShowBorder(false);

    var objFldRecordName = objForm.addField("custpage_record_name", "text",
      "Record Name", null, null);
    objFldRecordName.setDefaultValue(strRecordName);
    objFldRecordName.setDisplayType("hidden");

    objForm.addField("custpage_left_label1", "label", "", null,
      "custpage_header_group").setLayoutType("normal", "startcol")
      .setDisplayType("inline");

    var objFldHeader = objForm.addField("custpage_header", "richtext", "",
      null, "custpage_header_group").setLayoutType("normal",
        "startcol").setDisplayType("inline").setPadding(1);
    // objFldHeader.setDefaultValue("<b>Process is waiting for
    // execution</b>");

    objForm.addFieldGroup("custpage_image_group", "Image", null)
      .setShowBorder(false);

    objForm.addField("custpage_left_label2", "label", "", null,
      "custpage_image_group").setLayoutType("normal", "startcol")
      .setDisplayType("inline");

    var objFldImage = objForm.addField("custpage_image", "richtext", "",
      null, "custpage_image_group").setLayoutType("normal",
        "startcol").setDisplayType("inline").setPadding(3);

    var objFldStatusValue = objForm.addField("custpage_status_value",
      "text", "", null, null).setDisplayType("inline").setLayoutType(
        "normal", "startrow");
    objFldStatusValue.setDefaultValue("");
    objFldStatusValue.setDisplayType("hidden");

    // Adding a hidden field to form object
    var objFldProcessName = objForm.addField(
      "custpage_script_process_name", "text", "", null, null)
      .setDisplayType("hidden");
    var objFldScriptID = objForm.addField("custpage_script_id", "text", "",
      null, null).setDisplayType("hidden");
    var objFldScriptDeploymentID = objForm.addField(
      "custpage_script_deployment_id", "text", "", null, null)
      .setDisplayType("hidden");

    var objFldBackButtonLabel = null;
    var objFldBackScriptID = null;
    var objFldBackScriptDeploymentID = null;
    if (strBackScriptID && strBackScriptDeploymentID) {
      objFldBackButtonLabel = objForm.addField(
        "custpage_back_button_label", "text", "", null, null)
        .setDisplayType("hidden");
      objFldBackScriptID = objForm.addField("custpage_back_script_id",
        "text", "", null, null).setDisplayType("hidden");
      objFldBackScriptDeploymentID = objForm.addField(
        "custpage_back_script_deployment_id", "text", "", null,
        null).setDisplayType("hidden");
    }

    if (strScriptID && strScriptDeploymentID) {
      objFldProcessName.setDefaultValue(strProcessName);
      objFldScriptID.setDefaultValue(strScriptID);
      objFldScriptDeploymentID.setDefaultValue(strScriptDeploymentID);

      if (strBackScriptID && strBackScriptDeploymentID) {
        objFldBackButtonLabel.setDefaultValue(strBackButtonLabel);
        objFldBackScriptID.setDefaultValue(strBackScriptID);
        objFldBackScriptDeploymentID
          .setDefaultValue(strBackScriptDeploymentID);
      }

      // Query schedule script"s running instance
      var objArrInstanceResult = this.searchScheduleScriptInstance(
        strScriptID, strScriptDeploymentID, true);

      if (objArrInstanceResult) {
        nlapiLogExecution("DEBUG", "suitelet",
          "objArrInstanceResult[0].getValue('status'): " +
          objArrInstanceResult[0].getValue("status"));

        if (objArrInstanceResult[0].getValue("status") == "Pending") {
          objFldHeader
            .setDefaultValue("<font color='#5C74A0' size='2'><b>Scheduled process is waiting for Execution</b></font>");
          objFldImage.setDefaultValue("<img src='" +
            getFilePath(strImageName) + "'/>");
        } else if (objArrInstanceResult[0].getValue("status") == "Processing") {
          objFldHeader
            .setDefaultValue("<font color='#5C74A0' size='2'><b>" +
              strRecordName +
              " are being submitted.....</b></font>");
          objFldImage.setDefaultValue("<img src='" +
            getFilePath(strImageName) + "'/>");
        }

        objFldStatusValue.setDefaultValue(objArrInstanceResult[0]
          .getValue("status"));
        // objFldProgressValue.setDefaultValue(objArrInstanceResult[0].getValue("percentcomplete"));
      } else {
        objArrInstanceResult = this.searchScheduleScriptInstance(
          strScriptID, strScriptDeploymentID, false);

        if (objArrInstanceResult) {
          nlapiLogExecution("DEBUG", "suitelet",
            "objArrInstanceResult[0].getValue('status'): " +
            objArrInstanceResult[0]
              .getValue("status"));

          if (objArrInstanceResult[0].getValue("status") == "Complete") {
            objFldImage.setDefaultValue("");
            objFldHeader
              .setDefaultValue("<font color='#5C74A0' size='2'><b>" +
                strRecordName +
                " have been submitted</b></font>");
            if (strBackScriptID && strBackScriptDeploymentID) {
              objForm.addButton("custpage_back",
                strBackButtonLabel, "onBackClick();");
            }
          }

          objFldStatusValue.setDefaultValue(objArrInstanceResult[0]
            .getValue("status"));
        } else {
          objFldImage.setDefaultValue("");
          objFldHeader
            .setDefaultValue("<font color='#5C74A0' size='2'><b>" +
              strRecordName +
              " have been submitted</b></font>");

          objFldStatusValue.setDefaultValue("Complete");

          if (strBackScriptID && strBackScriptDeploymentID) {
            objForm.addButton("custpage_back", strBackButtonLabel,
              "onBackClick();");
          }
        }
      }
    }

    // Add submit button to the form object
    objForm.addSubmitButton("Refresh");
    // Displaying the page
    response.writePage(objForm);
  } catch (ex) {
    // TODO: handle exception
    nlapiLogExecution("ERROR", "suitelet", ex);
    retVal = null;
  }
}

/**
 * @param {String} -
 *            File Name.
 * @returns {String} - Returns url of the file
 */
function getFilePath(strFileName) {
  nlapiLogExecution("DEBUG", "getFilePath", "strFileName:" + strFileName);
  var retVal = null;

  try {
    var objArrSearchColumns = new Array();
    objArrSearchColumns[0] = new nlobjSearchColumn("internalid");
    objArrSearchColumns[1] = new nlobjSearchColumn("name");

    var objArrSearchFilters = new Array();
    objArrSearchFilters[0] = new nlobjSearchFilter("name", null, "is",
      strFileName);

    var objArrSearchResult = nlapiSearchRecord("file", null,
      objArrSearchFilters, objArrSearchColumns);

    if (objArrSearchResult && objArrSearchResult.length == 1) {
      var strFileID = objArrSearchResult[0]
        .getValue(objArrSearchColumns[0]);
      nlapiLogExecution("DEBUG", "getFilePath", "strFileID:" + strFileID);
      objFile = nlapiLoadFile(strFileID);

      retVal = objFile.getURL();
    }
  } catch (ex) {
    // TODO: handle exception
    nlapiLogExecution("ERROR", "getFilePath", ex);
    retVal = null;
  }

  nlapiLogExecution("DEBUG", "getFilePath", "retVal:" + retVal);
  return retVal;
}

/**
 * @param {String} -
 *            Script ID.
 * @param {String} -
 *            Script Deployment ID.
 * @param {Boolean} -
 *            If true status would check against "Pending" and "Processing" and
 *            others.
 * @returns {[]} - An array of nlobjSearchResult objects corresponding to the
 *          searched records.
 */
function searchScheduleScriptInstance(strScriptID, strScriptDeploymentID,
  isFlag) {
  nlapiLogExecution("DEBUG", "searchScheduleScriptInstance", "strScriptID:" +
    strScriptID + ", strScriptDeploymentID:" + strScriptDeploymentID +
    ", isFlag:" + isFlag);
  var retVal = null;

  try {
    // Defining search filters
    var objArrSearchFilters = new Array();
    if (isFlag) {
      objArrSearchFilters.push(new nlobjSearchFilter("status", null,
        "anyof", ["PENDING", "PROCESSING"]));
    }
    objArrSearchFilters.push(new nlobjSearchFilter("scriptid", "script",
      "is", strScriptID));

    // Defining search columns
    var objArrSearchColumns = new Array();
    objArrSearchColumns.push(new nlobjSearchColumn("status"));
    objArrSearchColumns.push(new nlobjSearchColumn("startdate")
      .setSort(true));
    objArrSearchColumns.push(new nlobjSearchColumn("enddate"));
    objArrSearchColumns.push(new nlobjSearchColumn("percentcomplete"));

    // Searching schedule script instance record
    var objArrScriptInstanceResult = nlapiSearchRecord(
      "scheduledscriptinstance", null, objArrSearchFilters,
      objArrSearchColumns);
    retVal = objArrScriptInstanceResult;

    // nlapiLogExecution("DEBUG", "searchScheduleScriptInstance",
    // "objArrScriptInstanceResult.length:" +
    // objArrScriptInstanceResult.length);
  } catch (ex) {
    // TODO: handle exception
    nlapiLogExecution("ERROR", "searchScheduleScriptInstance", ex);
    retVal = null;
  }
  return retVal;
}

/**
 * Client Script Code
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 *
 * @appliedtorecord recordType
 *
 * @param {String}
 *            type Access mode: create, copy, edit
 * @returns {Void}
 */

function clientPageInit(type) {
  // alert( "clientPageInit type:" + type);

  try {
    var strStatus = nlapiGetFieldValue("custpage_status_value"),
      refresh = function () {
        window.location.reload();
      };

    if (strStatus === "Pending" || strStatus === "Processing") {
      setTimeout(refresh, 5000);
    }
  } catch (ex) {
    // TODO: handle exception
    console.log("Error in clientPageInit :" + ex);
    alert("Error in clientPageInit :" + ex);
  }
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 *
 * @appliedtorecord recordType
 *
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientOnSave() {
  // alert( "clientOnSave:");
  var retVal = false;

  try {
    var strScriptID = nlapiGetFieldValue("custpage_script_id");
    var strScriptDeploymentID = nlapiGetFieldValue("custpage_script_deployment_id");
    var strProcessName = nlapiGetFieldValue("custpage_script_process_name");
    var strRecordName = nlapiGetFieldValue("custpage_record_name");

    var strScriptUrl = nlapiResolveURL('SUITELET', 'customscript_ss_script_execution_status', 'customdeploy_ss_script_execution_status', 'internal');
    // alert("clientOnSave strScriptUrl:" + strScriptUrl);
    strScriptUrl = strScriptUrl + "&custpage_script_id=" + strScriptID +
      "&custpage_script_deployment_id=" + strScriptDeploymentID +
      "&custpage_script_process_name=" + strProcessName +
      "&custpage_record_name=" + strRecordName;
    window.main_form.action = strScriptUrl;
    retVal = true;
  } catch (ex) {
    // TODO: handle exception
    console.log("Error in clientOnSave :" + ex);
    alert("Error in clientOnSave :" + ex);
  }

  // alert( "clientOnSave retVal:" + retVal);
  return retVal;
}

function onBackClick() {
  // alert( "onBackClick:");
  var retVal = false;

  try {
    // var strBackButtonLabel =
    // nlapiGetFieldValue("custpage_back_button_label");
    var strBackScriptID = nlapiGetFieldValue("custpage_back_script_id");
    var strBackScriptDeploymentID = nlapiGetFieldValue("custpage_back_script_deployment_id");

    var strScriptUrl = nlapiResolveURL('SUITELET', strBackScriptID, strBackScriptDeploymentID, 'internal');

    setWindowChanged(window, false);

    window.main_form.action = strScriptUrl;
    document.forms["main_form"].submit();

  } catch (ex) {
    // TODO: handle exception
    console.log("Error in onBackClick :" + ex);
    alert("Error in onBackClick :" + ex);
  }

  return retVal;
}
