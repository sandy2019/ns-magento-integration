/*******************************************************************
 *
 *
 * Name: mg.lirik.cs.automatedworkflowscreen.js
 * Script Type: ClientScript
 * @version: 2.7.1
 *
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 *
 * Author: Lirik Inc.
 * Purpose: Client script of Item Selection Screen for Sales Order.
 * Script:
 * Deploy:
 *
 *
 * ******************************************************************* */

define(['N/format', 'N/https', 'N/url', './lib/lodash_4.17.11_min'], client);

function client(format, https, url, _) {

  var CurrentRecord;
  var sublistId = 'form_sublist_so_items';

  var fieldsMap = {
    form_fld_shipping_method: 'form_sublist_col_lineitem_shipping_method',
    form_fld_item_package: 'form_sublist_col_lineitem_package',
    form_fld_item_status_code_1: 'form_sublist_col_lineitem_status_code_1',
    form_fld_item_schedule_qualifier_1: 'form_sublist_col_lineitem_schedule_qualifier_1',
    form_fld_item_schedule_date_1: 'form_sublist_col_lineitem_schedule_date_1',
    form_fld_item_acknowledgement_type: 'form_sublist_col_lineitem_acknowledgement_type'
  };

  var sublistFieldChange = ["form_sublist_col_lineitem_package", "form_sublist_col_lineitem_trackno", "form_sublist_col_lineitem_quantity", "form_sublist_col_lineitem_status_code_1", "form_sublist_col_lineitem_schedule_qty_1", "form_sublist_col_lineitem_schedule_qualifier_1", "form_sublist_col_lineitem_schedule_date_1", "form_sublist_col_lineitem_acknowledgement_type", "form_sublist_col_lineitem_shipping_method"];

  var validation = {
    form_sublist_col_lineitem_shipping_method: { name: 'Shipping Method', valueProperty: 'shipMethod' },
    form_sublist_col_lineitem_package: { name: 'Package', valueProperty: 'pckId' },
    form_sublist_col_lineitem_quantity: { name: 'Qty', valueProperty: 'qty' },
    form_sublist_col_lineitem_status_code_1: { name: 'Item Status Code 1', valueProperty: 'spsStatusCodeId' },
    form_sublist_col_lineitem_schedule_qty_1: { name: 'Item Schedule Qty 1', valueProperty: 'spsItemScheduleQty' },
    form_sublist_col_lineitem_schedule_qualifier_1: { name: 'Item Schedule Qualifier 1', valueProperty: 'spsItemScheduleQualId' },
    form_sublist_col_lineitem_schedule_date_1: { name: 'Item Schedule Date 1', valueProperty: 'spsItemScheduleDt' },
    form_sublist_col_lineitem_acknowledgement_type: { name: 'Acknowledgment Type', valueProperty: 'spsAckTypeId' }
  };

  function pageInit(scriptContext) {

    addStyle('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css', 'head');

    nsTabBackgroundColor = '#607799';
    nsTextOnTabColor = '#FFFFFF';

    // debugger;

    CurrentRecord = scriptContext.currentRecord;
    console.log('*** pageInit ***');
  }

  function addStyle(cssLink, pos) {
    var tag = document.getElementsByTagName(pos)[0];
    var addLink = document.createElement('link');
    addLink.setAttribute('type', 'text/css');
    addLink.setAttribute('rel', 'stylesheet');
    addLink.setAttribute('href', cssLink);
    tag.appendChild(addLink);
  }

  function customReset() {
    try {

      CurrentRecord.setValue({ fieldId: 'form_fld_customer', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_po', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_so', value: '', ignoreFieldChange: true, forceSyncSourcing: true });

      CurrentRecord.setValue({ fieldId: 'form_sublist_fld_pagination_curr_index', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_ship_date', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_item_status_code_1', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_item_schedule_qualifier_1', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_item_schedule_date_1', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_item_acknowledgement_type', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_shipping_method', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_item_package', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_pck_diff_item_sep', value: false, ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_pck_item_sep', value: false, ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_sublist_fld_sel_rec_count', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_sublist_fld_total_rec_count', value: '', ignoreFieldChange: true, forceSyncSourcing: true });

      CurrentRecord.setValue({ fieldId: 'form_fld_selected_records', value: '[]', ignoreFieldChange: true, forceSyncSourcing: true });

      setWindowChanged(window, false);
      document.forms.main_form.submit();

    } catch (err) {
      log.error({ title: 'client.customReset', details: err.name + ':' + err.message });
      throw err;
    }
  }

  function customSearch() {
    try {

      CurrentRecord.setValue({ fieldId: 'form_sublist_fld_pagination_curr_index', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_ship_date', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_item_status_code_1', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_item_schedule_qualifier_1', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_item_schedule_date_1', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_item_acknowledgement_type', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_shipping_method', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_item_package', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_pck_diff_item_sep', value: false, ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_fld_pck_item_sep', value: false, ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_sublist_fld_sel_rec_count', value: '', ignoreFieldChange: true, forceSyncSourcing: true });
      CurrentRecord.setValue({ fieldId: 'form_sublist_fld_total_rec_count', value: '', ignoreFieldChange: true, forceSyncSourcing: true });

      setWindowChanged(window, false);
      document.forms.main_form.submit();
    } catch (err) {
      log.error({ title: 'client.customSearch', details: err.name + ':' + err.message });
      throw err;
    }
  }

  function customSubmit() {
    try {

      var arrSelectedRecords = JSON.parse(CurrentRecord.getValue({ fieldId: 'form_fld_selected_records' }));

      if (!arrSelectedRecords || arrSelectedRecords.length < 1) {
        alert("Please select at least one order");
        return;
      }

      if (confirm("You are about to submit orders. Do you want to continue ?")) {

        CurrentRecord.setValue({ fieldId: 'form_fld_action', value: 'SUBMIT', ignoreFieldChange: true, forceSyncSourcing: true });

        setWindowChanged(window, false);
        document.forms.main_form.submit();
      }
    } catch (err) {
      log.error({ title: 'client.customSubmit', details: err.name + ':' + err.message });
      throw err;
    }
  }

  function fieldChanged(scriptContext) {
    try {

      if (_.has(fieldsMap, scriptContext.fieldId)) {

        var lineCount = CurrentRecord.getLineCount({ sublistId: sublistId });
        for (var line = 0; line < lineCount; line++) {

          CurrentRecord.selectLine({ sublistId: sublistId, line: line });
          CurrentRecord.setCurrentSublistValue({ sublistId: sublistId, fieldId: fieldsMap[scriptContext.fieldId], value: CurrentRecord.getValue({ fieldId: scriptContext.fieldId }), ignoreFieldChange: false, forceSyncSourcing: true });
        }

      } else if (scriptContext.fieldId === 'form_sublist_fld_pagination') {

        CurrentRecord.setValue({ fieldId: 'form_sublist_fld_pagination_curr_index', value: CurrentRecord.getValue({ fieldId: scriptContext.fieldId }), ignoreFieldChange: true, fireSyncSlaving: true });

        setWindowChanged(window, false);
        document.forms.main_form.submit();
      } else if (scriptContext.sublistId === sublistId) {

        var arrSelectedRecords = JSON.parse(CurrentRecord.getValue({ fieldId: 'form_fld_selected_records' }));
        var currSelectedItem = {
          id: CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_internalid', line: scriptContext.line }),
          lineUniqueKey: CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineuniquekey', line: scriptContext.line })
        };
        currSelectedItem.custId = CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_customer', line: scriptContext.line });
        currSelectedItem.item = CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_item', line: scriptContext.line });
        currSelectedItem.desc = CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_desc', line: scriptContext.line });
        currSelectedItem.shipMethod = CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_shipping_method', line: scriptContext.line });
        currSelectedItem.pckId = CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_package', line: scriptContext.line });
        currSelectedItem.trackno = CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_trackno', line: scriptContext.line });
        currSelectedItem.qty = parseInt(CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_quantity', line: scriptContext.line })).toString();
        currSelectedItem.spsStatusCodeId = CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_status_code_1', line: scriptContext.line });
        currSelectedItem.spsItemScheduleQty = parseInt(CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_schedule_qty_1', line: scriptContext.line })).toString();
        currSelectedItem.spsItemScheduleQualId = CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_schedule_qualifier_1', line: scriptContext.line });
        currSelectedItem.spsItemScheduleDt = CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_schedule_date_1', line: scriptContext.line }) ? format.format({ value: CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_schedule_date_1', line: scriptContext.line }), type: format.Type.DATE }) : '';
        currSelectedItem.spsAckTypeId = CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineitem_acknowledgement_type', line: scriptContext.line });

        if (scriptContext.fieldId === 'form_sublist_col_select') {

          var selRecordsCount = Number(CurrentRecord.getValue({ fieldId: 'form_sublist_fld_sel_rec_count' }));

          if (CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: scriptContext.fieldId, line: scriptContext.line })) {

            var validationKeys = Object.keys(validation);

            for (var index = 0; index < validationKeys.length; index++) {

              if (!CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: validationKeys[index], line: scriptContext.line })) {
                alert('Please provide required information. ' + validation[validationKeys[index]].name);
                CurrentRecord.setCurrentSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_select', value: false, ignoreFieldChange: true, forceSyncSourcing: true });
                return;
              }
            }

            arrSelectedRecords.push(currSelectedItem);
            selRecordsCount++;
          } else {

            if (_.find(arrSelectedRecords, { id: currSelectedItem.id, lineUniqueKey: currSelectedItem.lineUniqueKey })) {
              _.remove(arrSelectedRecords, { id: currSelectedItem.id, lineUniqueKey: currSelectedItem.lineUniqueKey });
              selRecordsCount--;
            }
          }

          CurrentRecord.setValue({ fieldId: 'form_sublist_fld_sel_rec_count', value: selRecordsCount, ignoreFieldChange: true, fireSyncSlaving: true });
        } else if (sublistFieldChange.indexOf(scriptContext.fieldId) !== -1) {

          var foundSelectedRecord = _.find(arrSelectedRecords, { id: currSelectedItem.id, lineUniqueKey: currSelectedItem.lineUniqueKey });

          if (scriptContext.fieldId !== 'form_sublist_col_lineitem_schedule_date_1' && foundSelectedRecord && !CurrentRecord.getSublistValue({ sublistId: sublistId, fieldId: scriptContext.fieldId, line: scriptContext.line })) {

            alert('You can not provide empty value for "' + validation[scriptContext.fieldId].name + '"');

            CurrentRecord.setCurrentSublistValue({ sublistId: sublistId, fieldId: scriptContext.fieldId, value: foundSelectedRecord[validation[scriptContext.fieldId].valueProperty], ignoreFieldChange: true, forceSyncSourcing: true });

            return;
          } else if (foundSelectedRecord) {
            _.remove(arrSelectedRecords, { id: foundSelectedRecord.id, lineUniqueKey: foundSelectedRecord.lineUniqueKey });
            arrSelectedRecords.push(currSelectedItem);
          }
        }

        CurrentRecord.setValue({ fieldId: 'form_fld_selected_records', value: JSON.stringify(arrSelectedRecords) });

      } else if (scriptContext.fieldId === 'form_fld_pck_diff_item_sep' && CurrentRecord.getValue({ fieldId: scriptContext.fieldId }) && CurrentRecord.getValue({ fieldId: 'form_fld_pck_item_sep' })) {

        CurrentRecord.setValue({ fieldId: 'form_fld_pck_item_sep', value: false, ignoreFieldChange: false, fireSyncSlaving: true });

      } else if (scriptContext.fieldId === 'form_fld_pck_item_sep' && CurrentRecord.getValue({ fieldId: scriptContext.fieldId }) && CurrentRecord.getValue({ fieldId: 'form_fld_pck_diff_item_sep' })) {

        CurrentRecord.setValue({ fieldId: 'form_fld_pck_diff_item_sep', value: false, ignoreFieldChange: false, fireSyncSlaving: true });

      }
    } catch (err) {
      log.error({ title: 'client.fieldChanged', details: err.name + ':' + err.message });
      throw err;
    }
  }

  function customMarkAll() {

    try {

      var lineCount = CurrentRecord.getLineCount({ sublistId: sublistId });
      for (var line = 0; line < lineCount; line++) {

        CurrentRecord.selectLine({ sublistId: sublistId, line: line });
        if (!CurrentRecord.getCurrentSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_select' })) {

          var validationKeys = Object.keys(validation);

          for (var index = 0; index < validationKeys.length; index++) {
            if (!CurrentRecord.getCurrentSublistValue({ sublistId: sublistId, fieldId: validationKeys[index] })) {
              alert('Please provide required information. ' + validation[validationKeys[index]].name);
              CurrentRecord.setCurrentSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_select', value: false, ignoreFieldChange: true, forceSyncSourcing: true });
              return;
            }
          }

          CurrentRecord.setCurrentSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_select', value: true, ignoreFieldChange: false, forceSyncSourcing: true });
        }
      }

    } catch (err) {
      log.error({ title: 'client.customMarkAll', details: err.name + ':' + err.message });
      throw err;
    }
  }

  function customUnmarkAll() {

    try {
      if (confirm("You are about to Unmark orders. Do you want to continue ?")) {

        var arrSelectedRecords = JSON.parse(CurrentRecord.getValue({ fieldId: 'form_fld_selected_records' }));

        var lineCount = CurrentRecord.getLineCount({ sublistId: sublistId });
        for (var line = 0; line < lineCount; line++) {

          CurrentRecord.selectLine({ sublistId: sublistId, line: line });

          var lineUniqueKey = CurrentRecord.getCurrentSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_lineuniquekey' });

          var foundItem = _.find(arrSelectedRecords, { lineUniqueKey: lineUniqueKey });
          if (foundItem) {
            CurrentRecord.setCurrentSublistValue({ sublistId: sublistId, fieldId: 'form_sublist_col_select', value: false, ignoreFieldChange: false, forceSyncSourcing: true });
          }
        }

        // CurrentRecord.setValue({ fieldId: 'form_fld_selected_records', value: '[]' });
      }
    } catch (err) {
      log.error({ title: 'client.customUnmarkAll', details: err.name + ':' + err.message });
      throw err;
    }
  }

  return {
    pageInit: pageInit,
    customReset: customReset,
    customSearch: customSearch,
    customSubmit: customSubmit,
    fieldChanged: fieldChanged,
    customMarkAll: customMarkAll,
    customUnmarkAll: customUnmarkAll
  };
}
