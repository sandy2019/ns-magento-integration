/*
 * Copyright (C) 2018 Lirik, Inc.
 * http://lirik.io
 * hello@lirik.io
 * All rights reserved
 */

/*******************************************************************
 *
 *
 * Name: lirik.utils.ss1.js
 * Script Type: Library
 * @version 2.1.1
 *
 * Author: Lirik Inc.
 * Purpose: Utility file.
 *
 *
 * ******************************************************************* */

var utils = {

  checkInteger: function (value) {
    try {
      return /^[-]*\d+$/.test(value);
    } catch (err) {
      nlapiLogExecution('ERROR', 'utils.checkInteger', err);
      throw err;
    }
  },

  debug: function (title, details) {
    try {
      if (typeof details === 'object') {
        nlapiLogExecution('DEBUG', title, JSON.stringify(details));
      } else {
        nlapiLogExecution('DEBUG', title, details);
      }
    } catch (err) {
      nlapiLogExecution('ERROR', 'utils.debug', err);
      throw err;
    }
  },

  formatData: function (params) {
    try {

      if (params.mapping.type === 'boolean') {
        return params.value === 'T' ? true : false;
      } else if (params.mapping.type === 'currency') {
        return Number(params.value).toFixed(2);
      } else if (params.mapping.type === 'number') {
        return Number(params.value);
      } else {
        return params.value;
      }
    } catch (err) {
      nlapiLogExecution('ERROR', 'utils.formatData', err);
      throw err;
    }
  },

  getCompanyAddress: function (_nsContext) {
    try {

      var _nsRecordCompanyInformation = nlapiLoadConfiguration('companyinformation');

      var companyAddress = this.getRecordFieldValues({
        _nsRecord: _nsRecordCompanyInformation,
        fieldsMapping: COMPANY_ADDRESS_MAPPING,
        isNoRecordDetails: true
      });

      var _nsRecordEmployeeId = _nsContext.getSetting('SCRIPT', 'custscript_ep_ship_employee');
      if (_nsRecordEmployeeId) {
        var employeeData = nlapiLookupField('employee', _nsRecordEmployeeId, ['email', 'mobilephone']);
        companyAddress.fields.email = employeeData.email ? employeeData.email : undefined;
        companyAddress.fields.phone = employeeData.mobilephone ? employeeData.mobilephone : companyAddress.fields.phone;
      }

      // companyAddress.fields.mode = 'test';
      companyAddress.fields.object = 'Address';

      nlapiLogExecution('DEBUG', 'utils.getCompanyAddress', 'companyAddress ::' + JSON.stringify(companyAddress));

      if (!companyAddress.fields.country) {
        throw nlapiCreateError('INVALID_COMPANY_ADDRESS', 'Company address is not valid, please provide valid address on Company Iinformation Setup page.', false);
      }

      return companyAddress.fields;
    } catch (err) {
      nlapiLogExecution('ERROR', 'utils.getCompanyAddress', 'err ::' + err);
      throw err;
    }
  },

  getRecordFieldValues: function (params) {
    try {
      nlapiLogExecution('DEBUG', 'utils.getRecordFieldValues', 'fieldsMapping ::' + JSON.stringify(params.fieldsMapping));
      nlapiLogExecution('DEBUG', 'utils.getRecordFieldValues', 'returnObject ::' + params.fields ? JSON.stringify(params.fields) : params.fields);

      var _nsRecord = params._nsRecord;
      var fieldsMapping = params.fieldsMapping;
      var returnObject = params.fields;
      if (!returnObject) {
        returnObject = {};
      }

      for (var key in fieldsMapping) {

        if (fieldsMapping.hasOwnProperty(key) && key === 'fields') {

          returnObject.fields = {};
          var arrFieldKeys = Object.keys(fieldsMapping.fields);
          for (var fieldKeyIndex = 0; fieldKeyIndex < arrFieldKeys.length; fieldKeyIndex++) {

            var fieldKey = arrFieldKeys[fieldKeyIndex];
            var fieldMap = fieldsMapping.fields[fieldKey];

            if (fieldMap.isText) {
              returnObject.fields[fieldKey] = _nsRecord.getFieldText(fieldMap.nsid) || null;
            } else {
              returnObject.fields[fieldKey] = _nsRecord.getFieldValue(fieldMap.nsid) || null;
            }

            returnObject.fields[fieldKey] = this.formatData({
              'mapping': fieldMap,
              'value': returnObject.fields[fieldKey]
            });
          }
        } else if (fieldsMapping.hasOwnProperty(key) && key === 'lineitems') {

          returnObject.lineitems = {};
          var arrLineItemKeys = Object.keys(fieldsMapping.lineitems);

          for (var lineItemKeyIndex = 0; lineItemKeyIndex < arrLineItemKeys.length; lineItemKeyIndex++) {

            var lineItemKey = arrLineItemKeys[lineItemKeyIndex];
            var lineItemMap = fieldsMapping.lineitems[lineItemKey];

            var lineItemName = lineItemKey;
            if (lineItemMap.name) {
              lineItemName = lineItemMap.name;
            }

            returnObject.lineitems[lineItemName] = [];

            var listItemLinesCount = _nsRecord.getLineItemCount(lineItemKey);

            for (var currline = 1; currline <= listItemLinesCount; currline++) {
              _nsRecord.selectLineItem(lineItemKey, currline);

              var lineItemData = {};
              var arrLineItemFieldKeys = Object.keys(lineItemMap.fields);
              for (var lineItemFieldKeyIndex = 0; lineItemFieldKeyIndex < arrLineItemFieldKeys.length; lineItemFieldKeyIndex++) {
                var lineItemFieldKey = arrLineItemFieldKeys[lineItemFieldKeyIndex];
                var lineItemFieldMap = lineItemMap.fields[lineItemFieldKey];

                if (lineItemFieldMap.isText) {
                  lineItemData[lineItemFieldKey] = _nsRecord.getLineItemText(lineItemKey, lineItemFieldMap.nsid, currline) || null;
                } else {
                  lineItemData[lineItemFieldKey] = _nsRecord.getCurrentLineItemValue(lineItemKey, lineItemFieldMap.nsid) || null;
                }

                lineItemData[lineItemFieldKey] = this.formatData({
                  'mapping': lineItemFieldMap,
                  'value': lineItemData[lineItemFieldKey]
                });
              }

              returnObject.lineitems[lineItemName].push(lineItemData);
            }
          }
        }
      }

      if (!params.isNoRecordDetails) {
        returnObject.recordId = _nsRecord.getId();
        returnObject.recordType = _nsRecord.getRecordType();
      }

      nlapiLogExecution('DEBUG', 'utils.getRecordFieldValues', 'returnObject ::' + JSON.stringify(returnObject));
      return returnObject;
    } catch (err) {
      nlapiLogExecution('ERROR', 'utils.getRecordFieldValues', 'err ::' + err);
      throw err;
    }
  },

  setFieldsValues: function (params) {
    try {
      debug('utils.setFieldsValues', { fields: params.fields });

      if (params.nsRecord && params.fields) {

        params.fields = JSON.parse(JSON.stringify(params.fields));

        for (var key in params.fields) {
          params.nsRecord.setFieldValue(key, params.fields[key]);
        }
      }
    } catch (err) {
      nlapiLogExecution('ERROR', 'utils.setFieldsValues', err);
      throw err;
    }
  },

  selectLine: function (params) {
    try {
      debug('utils.selectLine', { lineItemFields: params.lineItemFields, sublistId: params.sublistId, ignoreRecalc: params.ignoreRecalc, line: params.line });

      if (params.nsRecord && params.sublistId && params.line) {
        params.nsRecord.selectLineItem(params.sublistId, params.line);

        params.lineItemFields = JSON.parse(JSON.stringify(params.lineItemFields));

        for (var key in params.lineItemFields) {
          params.nsRecord.setCurrentLineItemValue(params.sublistId, key, params.lineItemFields[key]);
        }

        if (params.ignoreRecalc) {
          params.nsRecord.commitLineItem(params.sublistId, params.ignoreRecalc);
        } else {
          params.nsRecord.commitLineItem(params.sublistId);
        }
      }
    } catch (err) {
      nlapiLogExecution('ERROR', 'utils.selectLine', err);
      throw err;
    }
  },

  /**
   * Reschedule the script if Remaining Governace is less than limit.
   * @param {Object} params 
   * @param {Object} params.nsContext
   * @param {number} params.limit
   * @returns {void}
   */
  rescheduleScript: function (params) {

    try {

      if (params.nsContext.getRemainingUsage() < params.limit) {

        var state = nlapiYieldScript();

        if (state.status == 'FAILURE') {
          nlapiLogExecution('ERROR', 'checkScriptGovernance', 'Failed to yield script, exiting: Reason = ' + state.reason + ' /Size = ' + state.size);
          nlapiLogExecution('ERROR', 'checkScriptGovernance', 'state ::' + JSON.stringify(state));
          throw 'Failed to yield script';
        } else if (state.status == 'RESUME') {
          nlapiLogExecution('AUDIT', 'checkScriptGovernance', 'Resuming script because of ' + state.reason + '. Size = ' + state.size);
        }
        // state.status will never be SUCCESS because a success would imply a yield has occurred. The equivalent response would be yield
      }
    } catch (err) {
      // TODO: handle exception
      nlapiLogExecution('ERROR', 'checkScriptGovernance', err);
      throw err;
    }
  }
};
