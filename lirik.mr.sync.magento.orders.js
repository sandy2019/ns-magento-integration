/*
 * Copyright (C) 2021 Maholi Inc
 * https://www.maholi.com
 * All rights reserved
 * Developed for Maholi by: Sandeep Kumar
 *
 * Lirik, Inc.
 * http://lirik.io
 * hello@lirik.io
 */

/*******************************************************************
 *
 * Name: lirik.mr.sync.magento.orders.js 
 *
 * @NScriptType MapReduceScript
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @version: 2.1.1
 *
 * Author: Lirik, Inc.
 * Purpose: This script is used for syncing orders from Magento to NetSuite.
 * Script:
 * Deploy:
 * Changes : 2.1.1 Resolve issue related to address spacing
 * ******************************************************************* */
 define(['N/log', 'N/error', 'N/file', 'N/format', 'N/record', 'N/runtime', 'N/search', 'N/url', 'N/email', './lirik.connector.tba', './lib/lodash_4.17.15_min', './utils/lirik.utils'], function (log, error, file, format, record, runtime, search, url, email, TBA, _, utils) {

    const utility = new utils();
    /**
     * Marks the beginning of the map/reduce script execution.
     * 
     * Invokes the input stage. This function is invoked one time in the execution of the script.
     * 
     * @param {Object} inputContext
     * @param {boolean} inputContext.isRestarted 
     * @param {Object} inputContext.ObjectRef
     * @param {string | number} inputContext.ObjectRef.id
     * @param {string} inputContext.ObjectRef.type
     * @returns {Object[] | Object}
     */
    function getInputData(inputContext) {
        var title = 'getInputData';
        try {

            var configParams = utility.customPreferences();
            var ordersJSON = getMagentoOrderJSON(configParams);
            log.debug({ title: 'Orders Count', details: ordersJSON.length });

            return ordersJSON
        } catch (err) {
            log.error({ title: title, details: err });
            throw err;
        }
    }

    /**
     * Invokes the map stage.
     * 
     * If this entry point is used, the map function is invoked one time for each key/value pair provided by the getInputData(inputContext) function.
     * 
     * Optional, but if this entry point is skipped, the reduce(reduceContext) entry point is required.
     * 
     * @param {Object} context
     * @param {boolean} context.isRestarted
     * @param {number} context.executionNo
     * @param {Object[]} context.errors
     * @param {string} context.key
     * @param {string} context.value
     * @param {function} context.write
     * @returns {void}
     */
    function map(context) {
        const title = 'map';
        var orderData = JSON.parse(context.value);
        try {
            log.debug({ title: 'Map :: MagentoJSonData', details: JSON.stringify(orderData) });
            const orderID = orderData.increment_id;
            const status = orderData.status.toString();
            log.debug({ title: 'Map:: Order Id', details: orderID });

            if (utility.isNotEmptyAndhasOwnProperty(orderData, 'increment_id')) {

                log.debug({ title, details: "Order Number #" + orderData.increment_id + " has been initiated" });

                if (!utility.isOrderExistInNS({ OrderId: orderData.entity_id }) && status == 'processing') {

                    log.debug({ title, details: "Order Number #" + orderData.increment_id + " is processing now." });

                    ///Split the method calls
                    var lineItemsData = createJSONForOrderLineItem({ 'fields': orderData });
                    var bodyFieldsData = createJSONForOrder({ 'fields': orderData });
                    var addressData = createJSONforShipAndBillAddr({ 'fields': orderData });

                    //NOTE:: Create Gift Certificate
                    var arrAuthcodes = null;
                    var arrGiftCertId = validateAndCreateGiftCertificate({ 'fields': orderData });
                    if (arrGiftCertId && arrGiftCertId.length > 0)
                        arrAuthcodes = entryGiftCardPurchase({ 'giftCertIds': arrGiftCertId, 'entity': bodyFieldsData.entity });

                    log.debug({ title, details: 'lineItemsData :' + JSON.stringify(lineItemsData) });
                    log.debug({ title, details: 'bodyFieldsData :' + JSON.stringify(bodyFieldsData) });
                    log.debug({ title, details: 'Address :' + JSON.stringify(addressData) });

                    const orderId = utility.createSORecord({
                        'fields': bodyFieldsData,
                        'lineItems': lineItemsData,
                        'shippingAddress': addressData.shippingAddress,
                        'billingAddress': addressData.billingAddress,
                        'giftCardLines': arrAuthcodes
                    });
                    log.debug({ title, details: { orderId } });

                    if (orderId)
                        addRecordInNSMagentoTracking({ orderId: orderData['increment_id'], errMsg: 'Success' });

                } else {
                    log.debug({ title, details: "Order Number #" + orderData.increment_id + " is already exists" });
                }

            }
        }
        catch (err) {
            log.error({ title, details: "Error while Creating SO of magento ID- " + orderData['increment_id'] + "error name : " + err.name + " error message : " + err.message });
            addRecordInNSMagentoTracking({ orderId: orderData['increment_id'], errMsg: err.message })
            //NOTE:: to summarize
            context.write({
                key: orderData['increment_id'],
                value: 'Fail'
            });
        }
    }

    function addRecordInNSMagentoTracking(params) {
        const title = 'addRecordInNSMagentoTracking';
        var dateTime = format.format({ value: new Date(), type: format.Type.DATETIMETZ });
        var recordId = null;
        try {
            var magentoNStrackingSearchObj = search.create({
                type: "customrecord_magnto_order_tracking_logs",
                filters: [
                    ["name", "is", params.orderId]
                ],
                columns: [
                    search.createColumn({
                        name: "name",
                        sort: search.Sort.ASC,
                        label: "Name"
                    })
                ]
            });
            //   var searchResultCount = magentoNStrackingSearchObj.runPaged().count;
            magentoNStrackingSearchObj.run().each(function (result) {
                // .run().each has a limit of 4,000 results
                recordId = result.id;
                return true;
            });
            if (recordId) {
                let newRecord = record.load({ type: 'customrecord_magnto_order_tracking_logs', id: recordId, isDynamic: true, });
                if (utility.isNotEmptyAndhasOwnProperty(params, 'errMsg')) {
                    newRecord.setValue({ fieldId: 'custrecord_date_time', value: dateTime });
                }
                recordId = newRecord.save();
            } else {
                let newRecord = record.create({ type: 'customrecord_magnto_order_tracking_logs' });
                newRecord.setValue({ fieldId: 'name', value: params.orderId });
                newRecord.setValue({ fieldId: 'custrecord_date_time', value: dateTime });
                if (utility.isNotEmptyAndhasOwnProperty(params, 'errMsg')) {
                    newRecord.setValue({ fieldId: 'custrecord_error_detail', value: params.errMsg });
                }
                recordId = newRecord.save();
            }
        } catch (err) {
            log.error({ title, details: err.message });
        }
        return recordId;
    }

    /**
     * Invokes the reduce stage.
     * 
     * If this entry point is used, the reduce function is invoked one time for each key and list of values provided. Data is provided either by the map stage or, if the map stage is not used, by the getInputData stage.
     * 
     * Optional, but if this entry point is skipped, the map(mapContext) entry point is required.
     * 
     * @param {Object} context
     * @param {boolean} context.isRestarted
     * @param {number} context.executionNo
     * @param {Object[]} context.errors
     * @param {string} context.key
     * @param {string[]} context.values
     * @param {function} context.write
     * @returns {void}
     */
    function reduce(context) {

        const title = 'reduce';

        try {
            log.debug({ title, details: { key: context.key } });
            context.write({ key: context.key, value: context.values });
        } catch (err) {
            log.error({ title: 'reduce', details: err });
            throw err;
        }
    }

    /**
     * Invokes the summarize stage.
     * 
     * If the summarize entry point is used, the summarize function is invoked one time in the execution of the script.
     * 
     * @param {Object} context
     * @param {boolean} context.isRestarted
     * @param {number} context.concurrency
     * @param {Date} context.dateCreated
     * @param {number} context.seconds
     * @param {number} context.usage
     * @param {number} context.yields
     * @param {Object} context.inputSummary
     * @param {Date} context.inputSummary.dateCreated
     * @param {number} context.inputSummary.seconds
     * @param {number} context.inputSummary.usage
     * @param {string} context.inputSummary.error
     * @param {Object} context.mapSummary
     * @param {Date} context.mapSummary.dateCreated
     * @param {number} context.mapSummary.concurrency
     * @param {Object} context.mapSummary.keys
     * @param {function} context.mapSummary.keys.iterator
     * @param {number} context.mapSummary.seconds
     * @param {number} context.mapSummary.usage
     * @param {number} context.mapSummary.yields
     * @param {Object} context.mapSummary.errors
     * @param {function} context.mapSummary.errors.iterator
     * @param {Object} context.reduceSummary
     * @param {Date} context.reduceSummary.dateCreated
     * @param {number} context.reduceSummary.concurrency
     * @param {Object} context.reduceSummary.keys
     * @param {function} context.reduceSummary.keys.iterator
     * @param {number} context.reduceSummary.seconds
     * @param {number} context.reduceSummary.usage
     * @param {number} context.reduceSummary.yields
     * @param {Object} context.reduceSummary.errors
     * @param {function} context.reduceSummary.errors.iterator
     * @param {Object} context.output
     * @param {function} context.output.iterator
     */
    function summarize(context) {

        try {
            var successSummary = [];
            var failureSummary = [];

            context.output.iterator().each(function (key, value) {
                if (value == 'Success') { successSummary.push(key); }
                else { failureSummary.push(key); }
                return true;
            });
        }
        catch (e) {
            log.error("Summarize::Error", e);

        }
    }

    function createJSONForDiscountItem(params) {
        const title = 'createJSONForDiscountItem';
        try {
            if (utility.isNotEmptyAndhasOwnProperty(params.fields, 'coupon_code')) {
                let itemId = nsItemIdfromSKU({ 'sku': params.fields.coupon_code });

                if (!itemId) {
                  let discountJSON=  createDiscountItem({'coupon_code':params.fields.coupon_code});
                    return discountJSON;
                }
                else {
                    return {
                        "discountitem": itemId,
                        "discountrate": params.fields.discount_amount
                    }
                }
            } else if (utility.isNotEmptyAndhasOwnProperty(params.fields, 'applied_rule_ids')) {
                let itemId = nsItemIdfromUPCCode({ 'upccode': params.fields.applied_rule_ids.split(',')[0] });
                if (!itemId) {
                    let discountJSON=  createDiscountItem({'applied_rule_ids':params.fields.applied_rule_ids.split(',')[0]});
                    return discountJSON;
                }
                return {
                    "discountitem": itemId,
                    "discountrate": params.fields.discount_amount
                }
            }
            else {
                return null;
            }

        } catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }
    }

    function createDiscountItem(params) {
        const title = 'createDiscountItem';
        try {
            let ruleDataObj = null;
            let itemid = ''; let upccode = ''; let description = ''; let simple_action = ''; let discountAmount = 0;
            if (utility.isNotEmptyAndhasOwnProperty(params, 'coupon_code')) {
                ruleDataObj = mgntoRuleDetFrmCouponCode({ 'coupon_code': params.coupon_code });
                itemid = params.coupon_code;
                description = ruleDataObj.description;
                simple_action = ruleDataObj.simple_action;
                discountAmount = ruleDataObj.discount_amount;
            }
            else if (utility.isNotEmptyAndhasOwnProperty(params, 'applied_rule_ids')) {
                ruleDataObj = mgntoRuleDetFrmRuleId({ 'applied_rule_ids': params.applied_rule_ids });
                itemid = ruleDataObj.items[0].name;
                upccode = ruleDataObj.items[0].rule_id;
                description = 'UPC code is used to identify discount rule from magento, do not edit or replace';
                simple_action = ruleDataObj.items[0].simple_action;
                discountAmount = ruleDataObj.items[0].discount_amount;
            }
            let rate = null;
            if (simple_action == 'cart_fixed' || simple_action == 'by_fixed') {
                rate = discountAmount;
            }
            if (simple_action == 'by_percent' || simple_action == 'buy_x_get_y') {
                rate = discountAmount + '%';
            }

            let discountItemFields = {
                'itemid': itemid,
                'description': description,
                'upccode': upccode,
                'rate': rate,
                'nonposting': 'F',
                'account': 439
            }
            let discountItemId = utility.createRecord({ 'type': record.Type.DISCOUNT_ITEM, 'fields': discountItemFields });

            return {
                "discountitem": discountItemId,
                "discountrate": ruleDataObj.discount_amount
            }
        }
        catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }
    }
    function mgntoRuleDetFrmCouponCode(params) {
        const title = 'mgntoRuleDetFrmCouponCode';
        try {
            var configParams = utility.customPreferences();
            let tba = new TBA();
            tba.setConsumer({ key: configParams.CONSUMER_KEY, secret: configParams.CONSUMER_SECRET });
            tba.setToken({ key: configParams.TOKEN_KEY, secret: configParams.TOKEN_SECRET });
            let searchCriteria = "searchCriteria[filterGroups][0][filters][0][field]=code&searchCriteria[filterGroups][0][filters][0][value]=" + params.coupon_code + "&searchCriteria[filterGroups][0][filters][0][conditionType]=eq";

            let ruleJSON = tba.sendRequest({
                url: configParams.MAGENTO_URL + '/rest/V1/coupons/search?' + searchCriteria,
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'GET',
            });
            let ruleId = ruleJSON.items[0].rule_id;
            if (ruleId > 0) {

                let salesRule = tba.sendRequest({
                    url: configParams.MAGENTO_URL + '/rest/V1/salesRules/' + ruleId,
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: 'GET',
                });
                return salesRule;
            }

        } catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }

    }

    function mgntoRuleDetFrmRuleId(params) {
        const title = 'mgntoRuleDetFrmRuleId';
        try {
            var configParams = utility.customPreferences();
            let tba = new TBA();
            tba.setConsumer({ key: configParams.CONSUMER_KEY, secret: configParams.CONSUMER_SECRET });
            tba.setToken({ key: configParams.TOKEN_KEY, secret: configParams.TOKEN_SECRET });
            let searchCriteria = "searchCriteria[filterGroups][0][filters][0][field]=rule_id&searchCriteria[filterGroups][0][filters][0][value]=" + params.applied_rule_ids + "&searchCriteria[filterGroups][0][filters][0][conditionType]=eq";
            let salesRule = tba.sendRequest({
                url: configParams.MAGENTO_URL + '/rest/V1/salesRules/search?' + searchCriteria,
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'GET',
            });
            return salesRule;


        } catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }

    }


    function createJSONForOrderLineItem(params) {
        const title = 'createJSONForOrderLineItem';
        try {
            let arrLineItems = [];

            log.debug({ title, details: { "params": params.fields } });
            if (utility.isNotEmptyAndhasOwnProperty(params.fields, 'items')) {
                const lineItems = params.fields.items;

                var tax_id = nsTaxCode({ 'fields': params.fields });
                log.debug({ title, details: tax_id });
                for (var itemLine = 0; itemLine < lineItems.length; itemLine++) {

                    var itemObj = lineItems[itemLine];
                    var itemSKU = itemObj.sku;
                    var qty = itemObj.qty_ordered;
                    var magentoOrderItemId = itemObj.item_id;
                    var magentoOrderProductId = itemObj.product_id;
                    var price = itemObj.price;
                    var productType = itemObj.product_type;
                    var nsItemId = '';

                    nsItemId = nsItemIdfromSKU({ 'sku': itemSKU });

                    if (productType == 'bundle') {
                        arrLineItems.push({
                            "item": nsItemId,
                            "quantity": qty,
                            "custcol_magento_order_item_id": magentoOrderItemId,
                            "custcol_magento_product_id": magentoOrderProductId,
                            "taxcode": tax_id
                        });
                    } else if (productType != 'bundle') {
                        if (utility.isNotEmptyAndhasOwnProperty(itemObj, "parent_item")) {
                            //Do Nothing
                        } else {
                            arrLineItems.push({
                                "item": nsItemId,
                                "quantity": qty,
                                "rate": price,
                                "custcol_magento_order_item_id": magentoOrderItemId,
                                "custcol_magento_product_id": magentoOrderProductId,
                                "taxcode": tax_id
                            });
                        }

                    }
                }
                if(!tax_id)
                    arrLineItems.forEach(function(obj){ delete obj.taxcode });
                return arrLineItems;
            }
        } catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }
    }

    function nsTaxCode(params) {
        const title = 'nsTaxCode';
        try {
            //Note
            let addresses = params.fields.extension_attributes;
            let region_code = '';
            for (var i in addresses.shipping_assignments) {
                let singleAddres = addresses.shipping_assignments[i];
                let addressType = singleAddres['shipping'];
                let shippingAddrMagento = addressType['address'];

                if (shippingAddrMagento['address_type'] == "shipping") {
                    region_code = shippingAddrMagento.region_code;
                }
            }
            log.debug({ title: "region_code", details: region_code });
            let configParams = utility.customPreferences();
            let taxConfig = configParams.TAX_CONFIG;
            log.debug({ title: "TaxConfig", details: taxConfig });
            let taxCodesDet = JSON.parse(taxConfig);
            log.debug({ title: "taxCodes", details: taxCodesDet });
            let taxCode = null;
            let tax_id = null;
            if (region_code) {
                log.debug({ title: "taxCodes", details: taxCodesDet[0].tax_id });
                taxCode = taxCodesDet.filter(code => code.province.toString() == region_code.toString());
                if (taxCode && taxCode.length > 0) {
                    tax_id = taxCode[0].tax_id;
                    log.debug({ title: "taxCode.tax_id", details: tax_id });
                }
            }

            return tax_id
        }
        catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }
    }
    function nsItemIdfromUPCCode(params) {
        const title = 'nsItemIdfromUPCCode';
        let itemId = '';
        try {
            let itemSearchObj = search.create({
                type: "item",
                filters: [
                    ["upccode", "is", params.upccode.trim()],
                    "AND",
                    ["isinactive", "is", "F"]
                ],
                columns: [
                    search.createColumn({
                        name: "itemid",
                        sort: search.Sort.ASC,
                        label: "Name"
                    })
                ]
            });
            let searchResultCount = itemSearchObj.runPaged().count;
            itemSearchObj.run().each(function (result) {
                // .run().each has a limit of 4,000 results
                itemId = result.id;
                return true;
            });
            return itemId;
        }
        catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }
    }
    function nsItemIdfromSKU(params) {
        const title = 'nsItemIdfromSKU';
        let itemId = '';
        try {
            let itemSearchObj = search.create({
                type: "item",
                filters: [
                    ["itemid", "is", params.sku.trim()],
                    "AND",
                    ["isinactive", "is", "F"]
                ],
                columns: [
                    search.createColumn({
                        name: "itemid",
                        sort: search.Sort.ASC,
                        label: "Name"
                    })
                ]
            });
            let searchResultCount = itemSearchObj.runPaged().count;
            itemSearchObj.run().each(function (result) {
                // .run().each has a limit of 4,000 results
                itemId = result.id;
                return true;
            });
            return itemId;
        }
        catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }

    }


    /** 
       * @param {Object} params
       * @returns {string} 
       */
    function validateAndCreateGiftCertificate(params) {
        const title = "validateAndCreateGiftCertificate";
        try {

            const bodyFields = params.fields;

            let lineGifts = bodyFields.extension_attributes.gift_cards.length;
            var arrItemIds = [];
            for (let i = 0; i < lineGifts; i++) {
                let giftCode = bodyFields.extension_attributes.gift_cards[i].code;
                let giftAmount = bodyFields.extension_attributes.gift_cards[i].amount;
                let giftfields = {
                    'itemid': giftCode,
                    'displayname': giftCode,
                    'incomeaccount': '54',//MAHOLI
                    'liabilityaccount': '453',
                    'taxschedule': 1, //Non-Taxable
                };
                // log.debug({ title: " giftfields JSON", details: giftfields });
                let id = utility.createRecord({ 'fields': giftfields, 'type': record.Type.GIFT_CERTIFICATE_ITEM, 'sublist': 'price', 'price_1_': giftAmount });

                arrItemIds.push({ 'id': id, 'giftCode': giftCode });
                //log.debug({ title: "in arrItemIds", details: arrItemIds });

            }
            // log.debug({ title: "arrItemIds", details: arrItemIds });
            return arrItemIds;
        } catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }
    }

    function entryGiftCardPurchase(params) {
        const title = "entryGiftCardPurchase";
        try {

            var arrGiftCertDets = params.giftCertIds;
            log.debug({ title: " arrGiftCertDets", details: arrGiftCertDets });
            let bodyFieldsData = {
                "entity": params.entity,
                "memo": "Gift card purchase from Magento",
            };
            var lineItems = [];
            var arrAuthCodes = [];
            for (let i = 0; i < arrGiftCertDets.length; i++) {
                let custfieldLookUp = search.lookupFields({ type: search.Type.CUSTOMER, id: params.entity, columns: ['altname', 'email'] });
                lineItems.push({
                    "item": arrGiftCertDets[i].id,
                    "giftcertfrom": "Maholi",
                    "giftcertnumber": arrGiftCertDets[i].giftCode.toString().substring(0, 9),
                    "giftcertrecipientemail": custfieldLookUp.email,
                    "giftcertrecipientname": custfieldLookUp.altname
                });
                arrAuthCodes.push({ 'authcode': arrGiftCertDets[i].giftCode.toString().substring(0, 9) });
                log.debug({ title: " lineItems JSON", details: lineItems });
            }
            let giftOrderId = utility.createSORecord({
                'fields': bodyFieldsData,
                'lineItems': lineItems
            });
            if (giftOrderId) {
                let objRecordInv = record.transform({ fromType: record.Type.SALES_ORDER, fromId: giftOrderId, toType: record.Type.INVOICE, isDynamic: true });
                let invId = objRecordInv.save();
                if (invId) return arrAuthCodes;
            }

        } catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }
    }

    function getGiftAuthCode() {

    }

    function createJSONforShipAndBillAddr(params) {
        //NOTE:: Set Billing Address
        const title = 'createJSONforShipAndBillAddr';
        try {
            var billingAddrObj = null, shippingAddrObj = null;
            var orderData = params.fields;
            var billingAddrMagento = orderData.billing_address

            billingAddrObj = {
                'country': billingAddrMagento.country_id,
                'addr1': billingAddrMagento.street,
                'attention': billingAddrMagento.company,
                'addressee': billingAddrMagento.firstname + ' ' + billingAddrMagento.lastname,
                'city': billingAddrMagento.city,
                'addrphone': billingAddrMagento.telephone,
                'state': billingAddrMagento.region_code,
                'zip': billingAddrMagento.postcode,
            };


            var addresses = orderData.extension_attributes;
            for (var i in addresses.shipping_assignments) {
                var singleAddres = addresses.shipping_assignments[i];
                var addressType = singleAddres['shipping'];
                var shippingAddrMagento = addressType['address'];

                if (shippingAddrMagento['address_type'] == "shipping") {

                    shippingAddrObj = {
                        'country': shippingAddrMagento.country_id,
                        'addr1': shippingAddrMagento.street,
                        'attention': shippingAddrMagento.company,
                        'addressee': shippingAddrMagento.firstname + ' ' + shippingAddrMagento.lastname,
                        'city': shippingAddrMagento.city,
                        'addrphone': shippingAddrMagento.telephone,
                        'state': shippingAddrMagento.region_code,
                        'zip': shippingAddrMagento.postcode,
                        'email': shippingAddrMagento.email
                    }
                }
            }
            return { 'billingAddress': billingAddrObj, 'shippingAddress': shippingAddrObj };
        }
        catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }


    }

    function validateAndCreateCustomer(params) {
        const title = "validateAndCreateCustomer";
        try {

            const bodyFields = params.fields;
            var entity = null;
            var customerJSON = null;
            var currency = null;
            if (bodyFields.order_currency_code == 'USD') currency = 2;
            else if (bodyFields.order_currency_code == 'CAD') currency = 1;
            if (utility.isNotEmptyAndhasOwnProperty(bodyFields, 'customer_id')) {
                entity = utility.nsCustomerId({ "id": bodyFields.customer_id });
                log.debug({ title: 'Entity Check1', details: { entity } });
                if (!entity) {
                    let mgntoCustomerObj = magentoCustomerJSON({ id: bodyFields.customer_id });
                    if (mgntoCustomerObj) {
                        customerJSON = {
                            'isperson': 'T',
                            'firstname': mgntoCustomerObj['firstname'],
                            'lastname': mgntoCustomerObj['lastname'],
                            'email': mgntoCustomerObj['email'],
                            'currency': currency,
                            'custentity_magento_customer_id': mgntoCustomerObj['id'],
                            'custentity_is_magento_customer': true
                        };
                    }
                }
            }
            else if (bodyFields.customer_is_guest == 1) {
                entity = utility.nsCustomerId({ "email": bodyFields.customer_email });
                log.debug({ title: 'Entity Check2', details: { entity } });
                if (!entity) {
                    customerJSON = {
                        'isperson': 'T',
                        'firstname': bodyFields['billing_address'].firstname,
                        'lastname': bodyFields['billing_address'].lastname,
                        'email': bodyFields['customer_email'],
                        'currency': currency,
                        'custentity_magento_guest_cust_email': bodyFields['customer_email'],
                        'custentity_is_magento_customer': true
                    };
                }

            }
            if (customerJSON && !entity)
                entity = utility.createRecord({ 'fields': customerJSON, 'type': record.Type.CUSTOMER });


            log.debug({ title, details: { entity } });
            log.debug({ title: "Customer JSON", details: customerJSON });
            return entity;
        } catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }
    }

    function createJSONForOrder(params) {
        const title = 'createJSONForOrder';
        try {
            const bodyFields = params.fields;
            var entity = validateAndCreateCustomer(params);
            let ns_handling_cost = 0; let ns_shipping_cost = 0; let ns_handling_cost_taxAmount = 0;
            let discountItemObj = createJSONForDiscountItem(params);

            let paymentInfo = bodyFields.extension_attributes.payment_additional_info;
            let paymentMethod = paymentInfo.filter(payment => payment.key == 'method_title');
            log.debug({ title: 'Payment Method', details: JSON.stringify(paymentMethod) });
            let paymentMethodName = '';
            if (paymentMethod)
                paymentMethodName = paymentMethod[0].value;

            let paymentMethodCode = bodyFields.payment.method;

            let shipmethod = '';
            let shippingDesc = bodyFields.shipping_description;
            if (shippingDesc.indexOf('UPS Expedited') >= 0) shipmethod = '9186';
            if (shippingDesc.indexOf('UPS Express') >= 0) shipmethod = '9187';
            if (shippingDesc.indexOf('UPS Standard') >= 0) shipmethod = '9185';

            var fieldsOrders = {
                "entity": (entity && entity !== '') ? entity : '',
                "tranid": bodyFields.increment_id,
                "memo": "Magento Order",
                "custbody_is_magento_order": true,
                "custbody_magento_order_id": bodyFields.entity_id,
                "custbody_magento_orderstatus": bodyFields.status,
                "custbody_magento_order_document_no": bodyFields.increment_id,
                "custbody_mgnto_paymethod_name": paymentMethodName,
                "custbody_mgnto_paymethod_code": paymentMethodCode,
                "shipmethod": shipmethod, //bodyFields.shipping_description,
                "shippingcost": bodyFields.shipping_amount,
                "location": "1",
                "orderstatus": "B",
                "discountitem": (discountItemObj && discountItemObj != '') ? discountItemObj.discountitem : '',
                "discountrate": (bodyFields.discount_amount && bodyFields.discount_amount != '') ? bodyFields.discount_amount : ''
            };
            //log.debug({ title, details: { fieldsOrders } });
            return fieldsOrders;

        } catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }

    }

    //NOTE:: Get orders from Magento
    function getMagentoOrderJSON(params) {
        const title = 'getMagentoOrderJSON';
        var totalJSONData = null;
        var jsonBody = null;
        try {

            //NOTE:: Get Date Time
            var dateTime ='2021-08-24 20:56:37'; // utility.getFormattedDate({ 'hours': params.BUFFER_HOURS }); 
            log.debug('Filter Date Time', dateTime);
            var tba = new TBA();
            tba.setConsumer({ key: params.CONSUMER_KEY, secret: params.CONSUMER_SECRET });
            tba.setToken({ key: params.TOKEN_KEY, secret: params.TOKEN_SECRET });
            //NOTE: Search Criteria
            // var searchCriteria = "?searchCriteria[filter_groups][0][filters][0][field]=created_at&searchCriteria[filter_groups][0][filters][0][value]=" + dateTime + "&searchCriteria[filter_groups][0][filters][0][condition_type]=gteq&searchCriteria[filter_groups][1][filters][0][field]=status&searchCriteria[filter_groups][1][filters][0][value]=processing&searchCriteria[filter_groups][1][filters][0][condition_type]=eq&searchCriteria[current_page]=0";
            var searchCriteria = "?searchCriteria[filter_groups][0][filters][0][field]=created_at&searchCriteria[filter_groups][0][filters][0][value]=" + dateTime + "&searchCriteria[filter_groups][0][filters][0][condition_type]=gteq&searchCriteria[filter_groups][1][filters][0][field]=status&searchCriteria[filter_groups][1][filters][0][value]=processing&searchCriteria[filter_groups][1][filters][0][condition_type]=eq&searchCriteria[current_page]=1&searchCriteria[pageSize]=" + params.PAGE_SIZE;
            //NOTE:: Send request to mageneto
            var jsonBody = tba.sendRequest({
                url: params.MAGENTO_URL + '/rest/V1/orders' + searchCriteria,
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'GET',
            });

            //NOTE:: Push items to 
            if (jsonBody && jsonBody.items) {
                totalJSONData = JSON.parse(JSON.stringify(jsonBody)).items
            }
            var currentPage = 2;
            var totalPages = jsonBody.total_count;

            totalPages = Math.ceil(totalPages / Number(params.PAGE_SIZE));
            log.debug({
                title: 'total pages',
                details: totalPages
            });
            for (var page = currentPage; page <= totalPages; page++) {
                log.debug({
                    title: 'page',
                    details: page
                });
                var searchCriteria = "?searchCriteria[filter_groups][0][filters][0][field]=created_at&searchCriteria[filter_groups][0][filters][0][value]=" + dateTime + "&searchCriteria[filter_groups][0][filters][0][condition_type]=gteq&searchCriteria[filter_groups][1][filters][0][field]=status&searchCriteria[filter_groups][1][filters][0][value]=processing&searchCriteria[filter_groups][1][filters][0][condition_type]=eq&searchCriteria[current_page]=" + page + "&searchCriteria[pageSize]=" + params.PAGE_SIZE;
                //NOTE:: Send request to mageneto
                var jsonBody = tba.sendRequest({
                    url: params.MAGENTO_URL + '/rest/V1/orders' + searchCriteria,
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: 'GET',
                });
                if (jsonBody && jsonBody.items) {
                    totalJSONData = totalJSONData.concat(JSON.parse(JSON.stringify(jsonBody)).items)
                }
            }
            return totalJSONData;

        } catch (err) {
            log.error({ title, details: err.message });
            return false;
        }

    }

    /** 
    * @param {Object} params
    * @returns {string} 
    */
    function magentoCustomerJSON(params) {
        const title = 'magentoCustomerJSON';
        var jsonBody = null;
        try {
            var configParams = utility.customPreferences();
            var tba = new TBA();
            tba.setConsumer({ key: configParams.CONSUMER_KEY, secret: configParams.CONSUMER_SECRET });
            tba.setToken({ key: configParams.TOKEN_KEY, secret: configParams.TOKEN_SECRET });
            var jsonBody = tba.sendRequest({
                url: configParams.MAGENTO_URL + '/rest/V1/customers/' + params.id,
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'GET',
            });
            return jsonBody;
        } catch (err) {
            log.error({ title, details: err.message });
            throw err;
        }

    }



    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
});
