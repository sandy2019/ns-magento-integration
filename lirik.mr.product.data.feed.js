/*******************************************************************
 *
 *
 * Name: lirik.mr.prodcut.data.feed.js
 * Script Type: MapReduceScript
 * @version: 1.1.0
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType MapReduceScript 
 *
 * Author: Lirik Inc.(Bharat) :: Date - 7/26/2021
 * Purpose: Define Data Mapping as to what data elements pertinent to products will be published from NetSuite to Magento
 * Script: 
 * Deploy: 
 *
 *
 * ******************************************************************* */

define(['N/record', 'N/search', './lirik.connector.tba', './utils/lirik.utils'], mapReduce);

function mapReduce(record, search, TBA, utils) {

    const utility = new utils();
    /**
     * 
     * Marks the beginning of the map/reduce script execution. Invokes the input stage.
     * 
     * This function is invoked one time in the execution of the script.
     * 
     * @param {Object} inputContext
     * @param {boolean} inputContext.isRestarted 
     * @param {Object} inputContext.ObjectRef
     * @param {string | number} inputContext.ObjectRef.id
     * @param {string} inputContext.ObjectRef.type
     * @returns {Object[] | Object}
     */
    function getInputData(inputContext) {

        const title = 'getInputData';

        try {

            log.debug({ title, details: 'Processing...' });

            //var ue_filters = runtime.getCurrentScript().getParameter({name: 'custscript_filter_parameter'});
            return search.create({
                //type: "item",
                type: "assemblyitem",
                filters:
                    [
                        ["type", "anyof", "Assembly"],
                        "AND",
                        ["created", "on", "today"],
                        "AND",
                        ["isinactive", "is", "F"], //for new
                        //  "AND",
                        //    ["internalid","anyof","2042557"],
                        "AND",
                        ["custitem_mg_show_in_magento", "is", "T"],
                        "AND",
                        ["matrixchild", "is", "T"]
                    ],
                columns:
                    [
                        search.createColumn({ name: "itemid", sort: search.Sort.ASC, label: "Part Number (SKU)" }),
                        search.createColumn({ name: "internalid", label: "Internal ID" }),
                        search.createColumn({ name: "displayname", label: "displayname" }),
                        search.createColumn({ name: "feedname", label: "Feed Name" }),
                        search.createColumn({ name: "salesdescription", label: "Purchase Description" }),
                        search.createColumn({ name: "class", label: "Class" }),
                        search.createColumn({ name: "baseprice", label: "Price" }),
                        search.createColumn({ name: "weight", label: "Item Weight" }),
                        search.createColumn({ name: "unitstype", label: "UOM" }),
                        search.createColumn({ name: "upccode", label: "UPC Code" }),

                    ]
            });

        } catch (err) {
            log.error({ title: 'getInputData', details: err });
            throw err;
        }
    }

    /**
     * 
     * Invokes the map stage.
     * 
     * If this entry point is used, the map function is invoked one time for each key/value pair provided by the getInputData(inputContext) function.
     * 
     * Optional, but if this entry point is skipped, the reduce(reduceContext) entry point is required.
     * 
     * @param {Object} context
     * @param {boolean} context.isRestarted
     * @param {number} context.executionNo
     * @param {Object[]} context.errors
     * @param {string} context.key
     * @param {string} context.value
     * @param {function} context.write
     * @returns {void}
     */
    function map(context) {

        const title = 'map';

        try {
            /* NOTE :: map all variables */
            let itemConfig = JSON.parse(context.value);
            log.debug({ title, details: { itemConfig: itemConfig } });
            let itemId = itemConfig.id;
            log.debug({ title, details: { itemId: itemId } });
            var configParams = utility.customPreferences();
            log.debug({ title, details: { configParams } });

            let megentoMessage = {};

            if (itemConfig && itemId !== '') {
                let itemIdValue = (itemConfig.values.itemid !== '') ? itemConfig.values.itemid : '';
                var arrItemIdValue = itemIdValue.split(':');
                if (arrItemIdValue && arrItemIdValue.length > 1) {
                    itemIdValue = arrItemIdValue[1].trim();
                    log.debug({ title, details: { itemIdValue } });
                }

                //NOTE : VALIDATE ITEM SKU IS EXIST OR NOT
                if (!isProductExistInMagento({ "sku": itemIdValue, configParams })) {
                    megentoMessage = {
                        "product": {
                            "sku": itemIdValue,
                            "name": (itemConfig.values.displayname !== '') ? itemConfig.values.displayname : '',
                            "attribute_set_id": 4,
                            "price": (itemConfig.values.baseprice !== '') ? itemConfig.values.baseprice : '0.00',
                            "status": 1,
                            "visibility": 1,
                            "type_id": "simple",
                            "weight": (itemConfig.values.weight !== '') ? itemConfig.values.weight : '0.00',
                            "product_links": [],
                            "options": [],
                            "media_gallery_entries": [],
                            "tier_prices": [],
                            "custom_attributes": [
                                {
                                    "attribute_code": "short_description",
                                    "value": (itemConfig.values.salesdescription !== '') ? itemConfig.values.salesdescription : '',
                                },
                                {
                                    "attribute_code": "description",
                                    "value": (itemConfig.values.salesdescription !== '') ? itemConfig.values.salesdescription : '',
                                },
                                {
                                    "attribute_code": "sizes",
                                    "value": "116"
                                },
                                {
                                    "attribute_code": "weights",
                                    "value": (itemConfig.values.weight !== '') ? itemConfig.values.weight : '0.00',
                                }
                            ]
                        }
                    }
                    log.debug({ title, details: { megentoMessage } });

                    var tba = new TBA();
                    tba.setConsumer({ key: configParams.CONSUMER_KEY, secret: configParams.CONSUMER_SECRET });
                    tba.setToken({ key: configParams.TOKEN_KEY, secret: configParams.TOKEN_SECRET });
                    //NOTE:: Send request to mageneto
                    var jsonBody = tba.sendRequest({
                        url: configParams.MAGENTO_URL + '/rest/V1/products',
                        header: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'POST',
                        "data": JSON.stringify(megentoMessage)
                    });
                    log.debug({ title, details: { jsonBody } });
                    log.debug({ title, details: "Product has been created." });
                }                
            }
            
            //context.write({ key: context.key, value: { singleRecord, rows: arrData } });
        } catch (err) {
            log.error({ title: 'map', details: err });
            throw err;
        }
    }

    function isProductExistInMagento(params) {
        const title = 'isProductExistInMagento';
        let isExist = false;
        try {

            if (params) {
                var configParams = params.configParams;
                var tba = new TBA();
                tba.setConsumer({ key: configParams.CONSUMER_KEY, secret: configParams.CONSUMER_SECRET });
                tba.setToken({ key: configParams.TOKEN_KEY, secret: configParams.TOKEN_SECRET });
                //NOTE:: Send request to mageneto
                var jsonBody = tba.sendRequest({
                    url: configParams.MAGENTO_URL + '/rest/V1/products/' + params.sku,
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: 'GET',
                    //"data" : JSON.stringify(megentoMessage)
                });
                log.debug({ title, details: { jsonBody } });

                //NOTE:: Push items to 
                if (jsonBody && jsonBody.id) {
                    isExist = true;
                    log.debug({ title, details: "Product is already exists in Magento" });
                }
            }
            return isExist;
        } catch (err) {
            log.error({ title: 'isProductExistInMagento', details: { "err" : err.message } });
            return isExist;
            //throw err;
        }
    }



    /**
     * 
     * Invokes the reduce stage.
     * 
     * If this entry point is used, the reduce function is invoked one time for each key and list of values provided. Data is provided either by the map stage or, if the map stage is not used, by the getInputData stage.
     * 
     * Optional, but if this entry point is skipped, the map(mapContext) entry point is required.
     * 
     * @param {Object} context
     * @param {boolean} context.isRestarted
     * @param {number} context.executionNo
     * @param {Object[]} context.errors
     * @param {string} context.key
     * @param {string[]} context.values
     * @param {function} context.write
     * @returns {void}
     */
    function reduce(context) {

        let title = 'reduce';

        try {
            log.debug({ title, details: { context } });

        } catch (err) {

            log.error({ title: 'reduce', details: err });
            throw err;
        }
    }



    /**
     *
     * Invokes the summarize stage.
     * 
     * If the summarize entry point is used, the summarize function is invoked one time in the execution of the script.
     *
     * Optional
     * 
     * @param {Object} context
     * @param {boolean} context.isRestarted
     * @param {number} context.concurrency
     * @param {Date} context.dateCreated
     * @param {number} context.seconds
     * @param {number} context.usage
     * @param {number} context.yields
     * @param {Object} context.inputSummary
     * @param {Date} context.inputSummary.dateCreated
     * @param {number} context.inputSummary.seconds
     * @param {number} context.inputSummary.usage
     * @param {string} context.inputSummary.err
     * @param {Object} context.mapSummary
     * @param {Date} context.mapSummary.dateCreated
     * @param {number} context.mapSummary.concurrency
     * @param {Object} context.mapSummary.keys
     * @param {function} context.mapSummary.keys.iterator
     * @param {number} context.mapSummary.seconds
     * @param {number} context.mapSummary.usage
     * @param {number} context.mapSummary.yields
     * @param {Object} context.mapSummary.errors
     * @param {function} context.mapSummary.errors.iterator
     * @param {Object} context.reduceSummary
     * @param {Date} context.reduceSummary.dateCreated
     * @param {number} context.reduceSummary.concurrency
     * @param {Object} context.reduceSummary.keys
     * @param {function} context.reduceSummary.keys.iterator
     * @param {number} context.reduceSummary.seconds
     * @param {number} context.reduceSummary.usage
     * @param {number} context.reduceSummary.yields
     * @param {Object} context.reduceSummary.errors
     * @param {function} context.reduceSummary.errors.iterator
     * @param {Object[]} context.output
     */
    function summarize(context) {

        const title = 'summarize';

        try {

            log.debug({ title, details: { context } });

            log.debug({ title: 'Process {END}', details: (new Date()).toISOString() });
        } catch (err) {
            log.error({ title: 'summarize', details: err });
            throw err;
        }
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
}
