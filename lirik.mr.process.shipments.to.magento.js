/*******************************************************************
 *
 *
 * Name: lirik.mr.process.shipments.to.magento.js
 * Script Type: MapReduceScript
 * @version: 1.2.0
 *
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 * @NScriptType MapReduceScript 
 *
 * Author: Lirik Inc.(Sandeep) :: Date - 7/24/2021
 * Purpose: Define Data Mapping as to what data elements pertinent to products will be published from NetSuite to Magento
 * Script: 
 * Deploy: 
 *
 *
 * ******************************************************************* */

define(['N/record', 'N/search', './lirik.connector.tba', './utils/lirik.utils'], mapReduce);

function mapReduce(record, search, TBA, utils) {

    const utility = new utils();
    /**
     * 
     * Marks the beginning of the map/reduce script execution. Invokes the input stage.
     * 
     * This function is invoked one time in the execution of the script.
     * 
     * @param {Object} inputContext
     * @param {boolean} inputContext.isRestarted 
     * @param {Object} inputContext.ObjectRef
     * @param {string | number} inputContext.ObjectRef.id
     * @param {string} inputContext.ObjectRef.type
     * @returns {Object[] | Object}
     */
    function getInputData(inputContext) {

        const title = 'getInputData';

        try {

            log.debug({ title, details: 'Processing...' });

            //var ue_filters = runtime.getCurrentScript().getParameter({name: 'custscript_filter_parameter'});
            return search.create({
                type: "itemfulfillment",
                filters:
                    [
                        ["type", "anyof", "ItemShip"],
                        "AND",
                        ["mainline", "is", "T"],
                        "AND",
                        ["createdfrom.custbody_is_magento_order", "is", "T"],
                        "AND",
                        ["createdfrom.custbody_magento_order_id", "isnotempty", ""],
                        "AND",
                        ["status", "anyof", "ItemShip:C"],
                        "AND",
                        ["custbody_shipment_in_magento", "is", "F"],
                        "AND",
                        ["datecreated", "onorafter", "tendaysago"]
                    ],
                columns:
                    [
                        search.createColumn({ name: "entity", label: "Name" }),
                        search.createColumn({ name: "tranid", label: "Document Number" }),
                        search.createColumn({ name: "statusref", label: "Status" }),
                        search.createColumn({ name: "createdfrom", label: "Created From" })
                    ]
            });

        } catch (err) {
            log.error({ title: 'getInputData', details: err });
            throw err;
        }
    }

    /**
     * 
     * Invokes the map stage.
     * 
     * If this entry point is used, the map function is invoked one time for each key/value pair provided by the getInputData(inputContext) function.
     * 
     * Optional, but if this entry point is skipped, the reduce(reduceContext) entry point is required.
     * 
     * @param {Object} context
     * @param {boolean} context.isRestarted
     * @param {number} context.executionNo
     * @param {Object[]} context.errors
     * @param {string} context.key
     * @param {string} context.value
     * @param {function} context.write
     * @returns {void}
     */
    function map(context) {

        const title = 'map';

        try {
            /* NOTE :: map all variables */
            let searchResult = JSON.parse(context.value);
            let itemFulfillId = searchResult.id;
            let itemFulfillTranId = searchResult.values.tranid.value;
            let itemFulfillRecord = record.load({ type: record.Type.ITEM_FULFILLMENT, id: itemFulfillId, isDynamic: false });


            log.debug({ title: 'map :', details: { itemFulfillId: itemFulfillId } });
            log.debug({ title: 'map :', details: { itemFulfillTranId: itemFulfillTranId } });

            var configParams = utility.customPreferences();
            log.debug({ title, details: { configParams } });

            let salesOrderId = itemFulfillRecord.getValue('createdfrom');
            let soFields = search.lookupFields({ type: search.Type.SALES_ORDER, id: salesOrderId, columns: ['custbody_magento_order_id', 'currency'] });
            let magentoOrderId = soFields.custbody_magento_order_id;
            let soCurrency = soFields.currency[0].value;
            let ifTrackingNumber = itemFulfillRecord.getSublistValue({ sublistId: 'packageups', fieldId: 'packagetrackingnumberups', line: 0 });
            let ifShippingCarrier ='UPS';// itemFulfillRecord.getValue('shipcarrier');
            let ifShipMethod = itemFulfillRecord.getText('shipmethod');

            let numLines = itemFulfillRecord.getLineCount({ sublistId: 'item' });
            var magentoJSON = '{';
            magentoJSON += '"items": [';
            let itemDet = [];
            for (let count = 0; count < numLines; count++) {
                let qty = Number(itemFulfillRecord.getSublistValue({ sublistId: 'item', fieldId: 'quantity', line: count }));
                let itemName = itemFulfillRecord.getSublistValue({ sublistId: 'item', fieldId: 'itemname', line: count });
                let itemId= getProductIdFromMagento({configParams:configParams,sku:itemName});
                magentoJSON += '{';
                magentoJSON += '"order_item_id":"' + itemId + '",';
                magentoJSON += '"qty":"' + qty + '"';
                if (count < numLines-1)
                    magentoJSON += '},';
                else
                    magentoJSON += '}';
            }
            magentoJSON += ']';

            magentoJSON += '"tracks": [';
            magentoJSON += '{';
            magentoJSON += '"track_number":"' + ifTrackingNumber + '",';
            magentoJSON += '"title":"' + ifShipMethod + '",';
            if (ifShippingCarrier)
                magentoJSON += '"carrier_code":"' + ifShippingCarrier + '"';
            else
                magentoJSON += '"carrier_code":"' + '' + '"';

            magentoJSON += '}';
            magentoJSON += ']';
            magentoJSON += '}';

           

            log.debug({title:'JSON Data',details:JSON.stringify(magentoJSON)});
           var tba = new TBA();
            tba.setConsumer({ key: configParams.CONSUMER_KEY, secret: configParams.CONSUMER_SECRET });
            tba.setToken({ key: configParams.TOKEN_KEY, secret: configParams.TOKEN_SECRET });
            log.debug({title:'magentoOrderId',details:magentoOrderId});
            var shipmentId = tba.sendRequest({
                url: configParams.MAGENTO_URL + '/rest/V1/order/' + magentoOrderId + '/ship',
                header: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
                method: 'POST',
                data: JSON.stringify(magentoJSON)
            });
            log.debug({ title, details: { shipmentId } });
            if (shipmentId) {
                log.debug({ title: 'map - requestData :', details: { shipmentId: shipmentId } });
                var tba = new TBA();
                tba.setConsumer({ key: configParams.CONSUMER_KEY, secret: configParams.CONSUMER_SECRET });
                tba.setToken({ key: configParams.TOKEN_KEY, secret: configParams.TOKEN_SECRET });
                var jsonBody = tba.sendRequest({
                    url: configParams.MAGENTO_URL + '/rest/V1/shipment/' + shipmentId + '/emails',
                    header: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
                    method: 'POST',
                    data: ''
                });
                log.debug({ title, details: { jsonBody: jsonBody } });
                itemFulfillRecord.setValue({ fieldId: 'custbody_shipment_in_magento', value: true });
                var ifRecordId = itemFulfillRecord.save({ enableSourcing: true, ignoreMandatoryFields: true }); // Save Fulfillment Record
                log.debug({ title, details: { ifRecordId: ifRecordId } });
            }

            //context.write({ key: context.key, value: { singleRecord, rows: arrData } });
        } catch (err) {
            log.error({ title: 'map', details: err });
           // throw err;
        }
    }

    function getProductIdFromMagento(params) {
        const title = 'getProductIdFromMagento';
        let productId = '';
        try {

            if (params) {
                var configParams = params.configParams;
                var tba = new TBA();
                tba.setConsumer({ key: configParams.CONSUMER_KEY, secret: configParams.CONSUMER_SECRET });
                tba.setToken({ key: configParams.TOKEN_KEY, secret: configParams.TOKEN_SECRET });
                //NOTE:: Send request to mageneto
                var jsonBody = tba.sendRequest({
                    url: configParams.MAGENTO_URL + '/rest/V1/products/' + params.sku,
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: 'GET',
                    //"data" : JSON.stringify(megentoMessage)
                });
                log.debug({ title, details: { jsonBody } });

                //NOTE:: Push items to 
                if (jsonBody && jsonBody.id) {
                    productId=jsonBody.id;
                    log.debug({ title, details: "Product exists in Magento" });
                }
            }
            return productId;
        } catch (err) {
            log.error({ title: 'getProductIdFromMagento', details: { "err": err.message } });
            return productId;
            //throw err;
        }
    }



    /**
     * 
     * Invokes the reduce stage.
     * 
     * If this entry point is used, the reduce function is invoked one time for each key and list of values provided. Data is provided either by the map stage or, if the map stage is not used, by the getInputData stage.
     * 
     * Optional, but if this entry point is skipped, the map(mapContext) entry point is required.
     * 
     * @param {Object} context
     * @param {boolean} context.isRestarted
     * @param {number} context.executionNo
     * @param {Object[]} context.errors
     * @param {string} context.key
     * @param {string[]} context.values
     * @param {function} context.write
     * @returns {void}
     */
    function reduce(context) {

        let title = 'reduce';

        try {
            log.debug({ title, details: { context } });

        } catch (err) {

            log.error({ title: 'reduce', details: err });
            throw err;
        }
    }



    /**
     *
     * Invokes the summarize stage.
     * 
     * If the summarize entry point is used, the summarize function is invoked one time in the execution of the script.
     *
     * Optional
     * 
     * @param {Object} context
     * @param {boolean} context.isRestarted
     * @param {number} context.concurrency
     * @param {Date} context.dateCreated
     * @param {number} context.seconds
     * @param {number} context.usage
     * @param {number} context.yields
     * @param {Object} context.inputSummary
     * @param {Date} context.inputSummary.dateCreated
     * @param {number} context.inputSummary.seconds
     * @param {number} context.inputSummary.usage
     * @param {string} context.inputSummary.err
     * @param {Object} context.mapSummary
     * @param {Date} context.mapSummary.dateCreated
     * @param {number} context.mapSummary.concurrency
     * @param {Object} context.mapSummary.keys
     * @param {function} context.mapSummary.keys.iterator
     * @param {number} context.mapSummary.seconds
     * @param {number} context.mapSummary.usage
     * @param {number} context.mapSummary.yields
     * @param {Object} context.mapSummary.errors
     * @param {function} context.mapSummary.errors.iterator
     * @param {Object} context.reduceSummary
     * @param {Date} context.reduceSummary.dateCreated
     * @param {number} context.reduceSummary.concurrency
     * @param {Object} context.reduceSummary.keys
     * @param {function} context.reduceSummary.keys.iterator
     * @param {number} context.reduceSummary.seconds
     * @param {number} context.reduceSummary.usage
     * @param {number} context.reduceSummary.yields
     * @param {Object} context.reduceSummary.errors
     * @param {function} context.reduceSummary.errors.iterator
     * @param {Object[]} context.output
     */
    function summarize(context) {

        const title = 'summarize';

        try {

            log.debug({ title, details: { context } });

            log.debug({ title: 'Process {END}', details: (new Date()).toISOString() });
        } catch (err) {
            log.error({ title: 'summarize', details: err });
            throw err;
        }
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
}
