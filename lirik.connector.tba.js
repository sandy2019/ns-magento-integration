/*******************************************************************
 *
 * Name: lirik.connector.tba.js
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @version: 2.0.2
 * 
 * Author: Lirik, Inc.
 * Purpose: This library is used in TBA for requesting NetSuite restlet
 * Script: customscript_lirik_kb_sc_processrequest
 * Deploy: customdeploy_lirik_kb_sc_processrequest
 *
 * ******************************************************************* */

define(['N/error', 'N/https', './utils/crypto-js_3.1.9-1_min', './utils/oauth-1.0a'], function (error, https, CryptoJS, OAuth) {

  return function () {

    this.token = { key: '', secret: '' };
    this.consumer = { key: '', secret: '' };

    this.setToken = function (params) {

      try {

        if (params.key && params.secret) {
          this.token.key = params.key;
          this.token.secret = params.secret;
        } else {
          throw error.create({ name: 'CUSTOM_INVALID_TOKEN', message: 'Please provide valid key & secret for token.', notifyOff: false });
        }
      } catch (error) {
        log.error({ title: 'TBA.setToken', details: error });
        throw error;
      }
    };

    this.setConsumer = function (params) {

      try {

        if (params.key && params.secret) {
          this.consumer.key = params.key;
          this.consumer.secret = params.secret;
        } else {
          throw error.create({ name: 'CUSTOM_INVALID_CONSUMER', message: 'Please provide valid key & secret for consumer.', notifyOff: false });
        }
      } catch (error) {
        log.error({ title: 'TBA.setConsumer', details: error });
        throw error;
      }
    };

    this.sendRequest = function (params) {

      try {

        var oauth = OAuth({
          consumer: this.consumer,
          // signature_method: 'HMAC-SHA1',
          signature_method: 'HMAC-SHA256',
          realm: params.accountId,
          hash_function: function (base_string, key) {
            return CryptoJS.HmacSHA256(base_string, key).toString(CryptoJS.enc.Base64);
          }
        });

        var request = {
          url: params.url,
          method: params.method === 'POST' ? params.method : 'GET'
        };

        var header = params.header ? JSON.parse(JSON.stringify(params.header)) : {};
        header.Authorization = oauth.toHeader(oauth.authorize(request, this.token)).Authorization;
        // log.debug({ title: 'TBA.sendRequest', details: { header: header } });

        var response = null;
        if (params.method === 'POST') {
          response = https.post({ url: request.url, body: params.data, headers: header });
        } else {
          response = https.get({ url: request.url, headers: header });
        }
        // log.debug({ title: 'TBA.sendRequest', details: { response: response } });
        log.debug({ title: 'TBA.sendRequest', details: { resCode: response.code } });
        log.debug({ title: 'TBA.sendRequest', details: "resBody ::" + response.body });

        if (response.code && (Number(response.code) === 200 || Number(response.code) === 201)) {
          return JSON.parse(response.body);
        } else {

          var responseError = null;
          try {
            responseError = JSON.parse(response.body);
          } catch (error) {
            throw error.create({ name: 'CUSTOM_NETSUITE_REQUEST_FAILED', message: response.body, notifyOff: false });
          }

          if (responseError.error) {
            throw error.create({ name: responseError.error.code, message: responseError.error.message, notifyOff: false });
          } else {
            throw error.create({ name: 'CUSTOM_NETSUITE_REQUEST_FAILED', message: response.body, notifyOff: false });
          }
        }
      } catch (error) {
        log.error({ title: 'TBA.sendRequest', details: error });
        throw error;
      }
    };
  };
});
